%{
    #include <stdio.h>
    #include <string.h>
	#include "assign2.h"
	int lineNo;
	int columnNo;
    int yylex(void);
    void yyerror(char *);
    int varCountArr[1000];
    extern FILE *yyin;
	FILE *outFile;
%}

%token COPERATORS
%token DEFINEC
%token DEFINE
%token ELSEC
%token ELSEIF
%token ELSE
%token ELIFC
%token ENDC
%token ENDIF
%token ERRORC
%token ERROR
%token FATAL
%token HPLUS
%token HMINUS
%token LONGSTRINGS
%token IFNDEF
%token IFDEF
%token IFOPT
%token IFC
%token IF
%token INCLUDEPATH
%token INCLUDE
%token INFO
%token INLINE
%token IPLUS
%token IMINUS
%token I
%token LINKLIB
%token LINK
%token L
%token MACRO
%token MESSAGE
%token STATIC
%token STOP
%token UNDEFC
%token UNDEF
%token WARNINGS
%token WARNING
%token WARN
%token OBJECTPATH


%token LEFTCURLY
%token RIGHTCURLY
%token LEFTBSTAR
%token RIGHTBSTAR


%token COMMENT

%token <strVal> STR

%token END_OF_PROGRAM


%token CE
%token LG
%token LL
%token GG
%token GE
%token LE
%token PL
%token ML
%token PG
%token MG
%token SL
%token DL
%token SG
%token DG
%token SS
%token PLUS
%token MINUS
%token MUL
%token DIVIDE
%token EQUAL
%token LESS
%token GREATER
%token AMPERSAND
%token PIPE
%token TILDA
%token BANG
%token COLON
%token SEMICOLON
%token COMMA
%token LPAREN
%token RPAREN
%token LBRACKET
%token RBRACKET
%token CAPDOT
%token CAP
%token DOTDOT
%token DOT
%token AT

%token <intVal> HEXADECIMAL
%token <intVal> OCTAL
%token <intVal> BINARY
%token <floatVal> EXPONENTIAL
%token <floatVal> FLOAT
%token <intVal> INT

%token CHAR
%token ABSOLUTE
%token AND
%token ARRAY
%token ASM
%token BEGINP
%token CASE
%token CONST
%token CONSTRUCTOR
%token DESTRUCTOR
%token DIV
%token DO
%token DOWNTO
%token END
%token FILEP
%token FOR
%token FUNCTION
%token GOTO
%token IMPLEMENTATION
%token IN
%token INHERITED
%token INTERFACE
%token LABEL
%token MOD
%token NIL
%token NOT
%token OBJECT
%token OF
%token OPERATOR
%token OR
%token PROCEDURE
%token PROGRAM
%token RECORD
%token REINTRODUCE
%token REPEAT
%token SELF
%token SET
%token SHL
%token SHR
%token STRING
%token THEN
%token TO
%token TYPE
%token UNIT
%token UNTIL
%token USES
%token VAR
%token WHILE
%token WITH
%token XOR
%token DISPOSE
%token FALSE
%token TRUE
%token EXIT
%token NEW
%token AS
%token CLASS
%token DISPINTERFACE
%token EXCEPT
%token EXPORTS
%token FINALIZATION
%token FINALLY
%token INTIALIZATION
%token IS
%token LIBRARY
%token ON
%token OUT
%token BITPACKED
%token PROPERLY
%token RAISE
%token RESOURCESTRING
%token THREADVAR
%token TRY
%token ABSTRACT
%token ALIAS
%token ASSEMBLER
%token PACKED
%token BREAK
%token CDECL
%token CONTINUE
%token CPPDECL
%token CVAR
%token DEFAULT
%token DEPRECATED
%token DYNAMIC
%token ENUMERATOR
%token EXPERIMENTAL
%token EXPORT
%token EXTERNAL
%token FAR
%token FAR16
%token FORWARD
%token GENERIC
%token HELPER
%token IMPLEMENTS
%token INDEX
%token INTERRUPT
%token IOCHECKS
%token LOCAL
%token NAME
%token NEAR
%token NODEFAULT
%token NORETURN
%token NOSTACKFRAME
%token OLDFPCCALL
%token OTHERWISE
%token OVERLOAD
%token OVERRIDE
%token PASCAL
%token PLATFORM
%token PRIVATE
%token PROTECTED
%token PUBLIC
%token PUBLISHED
%token READ
%token REGISTER
%token RESULT
%token SAFECALL
%token SAVEREGISTERS
%token SOFTFLOAT
%token SPECIALIZE
%token STDCALL
%token STORED
%token STRICT
%token UNALLIGNED
%token UNIMPLEMENTED
%token VARARGS
%token VIRTUAL
%token WRITE
%token INTEGER
%token CARDINAL
%token SHORTINT
%token SMALLINT
%token LONGINT
%token INT64
%token BYTE
%token LONGWORD
%token WORD
%token EXTENDED
%token BOOLEAN
%token REAL
%token CONST_CHAR

%token IDENTIFIER

%token NEWLINE
%token SPACE

%token BADIDENTIFIER
%token ALIEN

%union {
    char* strVal;
    long long int intVal;
    double floatVal;
}
%start file

%%
file:
    PROGRAM IDENTIFIER cmdArguments SEMICOLON initList BEGINP stmtList END_OF_PROGRAM {;} 
    ;     
cmdArguments:
	LPAREN identifierList RPAREN {;}
	|
	/* epsilon*/	{;}
	; 
identifierList:    //cannot be empty
	IDENTIFIER COMMA identifierList  {;}
	|
	IDENTIFIER      {;}
	;
initList:
	varBody	initList {;}
	|
	typeBody initList
	| 
	constBody initList
	|
	procBody initList
	|
	funcBody initList
	|
	/* epsilon */
	;
varBody:
	VAR varDeclarations
	;
varDeclarations:
	varDeclaration varDeclarations
	|
	varDeclaration
	;
varDeclaration:
	identifierList COLON dataType SEMICOLON
	|
	IDENTIFIER COLON initializedDataType SEMICOLON 
	; 
typeBody:
	TYPE typeDeclarations
	;
typeDeclarations:
	typeDeclaration typeDeclarations
	|
	typeDeclaration
	;
typeDeclaration:
	IDENTIFIER EQUAL dataType SEMICOLON
	;
constBody:
	CONST constDeclarations
	;
constDeclarations:
	constDeclaration constDeclarations
	|
	constDeclaration
	;
constDeclaration:
	IDENTIFIER EQUAL compileTimeExpr SEMICOLON
	|
	IDENTIFIER EQUAL compileTimeStringExpr SEMICOLON
	|
	IDENTIFIER EQUAL NIL SEMICOLON
	|
	IDENTIFIER EQUAL range SEMICOLON;
	;
compileTimeExpr:
	LPAREN compileTimeExpr RPAREN
	|
	compileTimeExpr op compileTimeExpr
	|
	unaryOp compileTimeExpr
	|
	number
	;
compileTimeStringExpr:
	LPAREN compileTimeStringExpr RPAREN
	|
	compileTimeStringExpr PLUS compileTimeStringExpr
	|
	string
	;
op:
	addOp
	|
	mulOp
	|
	relOp
	;
addOp:
	PLUS
	|
	MINUS
	|
	OR
	|
	XOR
	;
mulOp:
	MUL
	|
	DIVIDE
	|
	DIV
	|
	MOD
	|
	AND
	|
	AS
	|
	SHL
	|
	SHR
	|
	LL
	|
	GG
	;
relOp:
	EQUAL
	|
	LG
	|
	LESS
	|
	GREATER
	|
	IN
	|
	IS
	|
	LE
	|
	GE
	;
unaryOp:
	NOT
	|
	AT
	|
	PLUS
	|
	MINUS
	;
number:
	constantNum
	|
	constantFloat
	|
	IDENTIFIER
	;
constantNum:
	INT
	|
	OCTAL
	|
	HEXADECIMAL
	|
	BINARY
	;
constantFloat:
	FLOAT
	|
	EXPONENTIAL
	;
enumDeclaration:
	LPAREN identifierList RPAREN
	;
arrayDeclaration:
	ARRAY LBRACKET range RBRACKET OF dataType
	;
range:
	constantNum DOTDOT constantNum
	|
	IDENTIFIER DOTDOT IDENTIFIER
	|
	constantNum DOTDOT IDENTIFIER
	|
	IDENTIFIER DOTDOT constantNum
	|
	IDENTIFIER
	;
recordDeclaration:
	RECORD recordList END
	;
recordList: /*can be empty*/
	recordEntry recordList
	|
	/*epsilon*/
	;
recordEntry:
	IDENTIFIER COLON dataType SEMICOLON
	;
procBody:
	procDeclaration initList block
	;
procDeclaration:
	PROCEDURE IDENTIFIER LPAREN argumentList RPAREN SEMICOLON
	;
funcBody:
	funcDeclaration initList block
	;
funcDeclaration:
	FUNCTION IDENTIFIER LPAREN argumentList RPAREN COLON dataTypes SEMICOLON
	;
argumentList:
	/*epsilon*/
	|
	arguments
	;
arguments:
	refParameter IDENTIFIER COLON dataTypes SEMICOLON arguments
	|
	refParameter IDENTIFIER COLON dataTypes
	;
refParameter: /* VAR is used for passing by reference */
	VAR
	|
	/*epsilon*/
	;
dataType:
	CAP dataTypes
	|
	dataTypes
	|
	arrayDeclaration
	|
	recordDeclaration
	|
	enumDeclaration
	;
dataTypes:
	CHAR
	|
	INTEGER
	|
	CARDINAL
	|
	SHORTINT
	|
	LONGINT
	|
	INT64
	|
	BYTE
	|
	WORD
	|
	LONGWORD
	|
	EXTENDED
	|
	BOOLEAN
	|
	REAL
	|
	STRING
	|
	STRING LBRACKET count RBRACKET
	|
	IDENTIFIER
	;
initializedDataType:
	CAP dataTypes EQUAL NIL /* ^string[34] is allowed here but not in fpc*/
	|
	initializedDataTypes
	;
initializedDataTypes:
	CHAR EQUAL CONST_CHAR
	|
	INTEGER EQUAL compileTimeExpr
	|
	CARDINAL EQUAL compileTimeExpr
	|
	SHORTINT EQUAL compileTimeExpr
	|
	LONGINT EQUAL compileTimeExpr
	|
	INT64 EQUAL compileTimeExpr
	|
	WORD EQUAL compileTimeExpr
	|
	BYTE EQUAL compileTimeExpr
	|
	LONGWORD EQUAL compileTimeExpr
	|
	EXTENDED EQUAL compileTimeExpr
	|
	REAL EQUAL compileTimeExpr
	|
	BOOLEAN EQUAL TRUE
	|
	BOOLEAN EQUAL FALSE
	|
	STRING EQUAL string
	|
	STRING LBRACKET count RBRACKET EQUAL string
	|
	dataTypes EQUAL IDENTIFIER
	;
count:
	count addOp count
	|
	constantNum
	|
	IDENTIFIER
	;
block:
	BEGINP stmtList END SEMICOLON
	;
string:
	STR
	|
	CONST_CHAR
	|
	IDENTIFIER
	;
stmtList:
	/*epsilon*/
	;
	

%%
void yyerror(char *s) {
     fprintf(stderr, "%s\n", s);
}

int main(int argv, char *argc[]){
    lineNo = columnNo = 0 ;
	for (int i=0;i<1000;i++)
		varCountArr[i] = 0;
	yyin = fopen(argc[1],"r");
	if(yyin == NULL){
		printf("unable to open file %s\n",argc[1] );
		return 0;
	}
	int i;
	char outFileName[105];
	for(i=0;argc[1][i] != '\0' && argc[1][i] != '.' && (i < 100) ;i++)
		outFileName[i] = argc[1][i];
	strcat(outFileName,".dot\0");
	outFile = fopen(outFileName,"w");
	if(outFile == NULL){
		printf("unable to open file %s\n",outFileName );
		return 0;
	}
	fprintf(outFile, "digraph {rankdir=TB; \n");
	yydebug = 1;
	yyparse();
	fclose(yyin);
	fprintf(outFile, "}\n" );
	fclose (outFile);
	return 0;
}
