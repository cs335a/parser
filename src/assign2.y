%{
    #include <stdio.h>
    #include <iostream>
    #include <stack>
    #include <string>
    #include <cstring>
    #include <sstream>
    #include <fstream>
	#include "assign2.h"
	int lineNo;
    int yylex(void);
    void yyerror(char *);
    int varCountArr[1000];
    extern FILE *yyin;
	std::ofstream outFile;
    int count = 0;
    std::stack<int> currStack;
    using namespace std;
    struct pairVal{
        string str;
        int val;
    };
    std::stack < struct pairVal > tempStack;

    void printStackContent(){
        cout << "Printing stack contents:\n";
        for (std::stack<int> dump = currStack; !dump.empty(); dump.pop()){
            cout << dump.top();
            if(dump.size() != 1){
                cout << ", ";
            }
            else {
                cout << endl;
            }
        }
        cout << "(" << currStack.size() << " elements)\n";
    }

    string toWrite(string type){
        //printStackContent();
        struct pairVal tempVar;
        if(!type.compare("file")){
            /*PROGRAM IDENTIFIER cmdArguments SEMICOLON initList BEGINP statementList END_OF_PROGRAM*/
            tempVar.val = count++;
            tempVar.str = "END_OF_PROGRAM";
            tempStack.push(tempVar);

            tempVar.val = currStack.top();
            tempVar.str = "statementList";
            currStack.pop();
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "BEGINP";
            tempStack.push(tempVar);

            tempVar.val = currStack.top();
            tempVar.str = "initList";
            currStack.pop();
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "SEMICOLON";
            tempStack.push(tempVar);

            tempVar.val = currStack.top();
            tempVar.str = "cmdArguments";
            currStack.pop();
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "IDENTIFIER";
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "PROGRAM";
            tempStack.push(tempVar);
        }
        else if(!type.compare("epsilon")){
            /*epsilon*/
            tempVar.val = count++;
            tempVar.str = "EPSILON";
            tempStack.push(tempVar);
        }
        else if(!type.compare("cmdArguments1")){
        	tempVar.val = count++;
            tempVar.str = "RPAREN";
            tempStack.push(tempVar);

            tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

            tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("identifierList1")){
			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("identifierList2")){
        	tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("initList1")){
			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "varBody";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("initList2")){
			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "typeBody";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("initList3")){
			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "constBody";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("initList4")){
			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "procDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("initList5")){
			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "funcDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("initList6")){
			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "operatorDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("initList7")){
			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "constructorDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("initList8")){
			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "destructorDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("varBody1")){
			tempVar.val = currStack.top();
			tempVar.str = "varDeclarations";
			currStack.pop();
			tempStack.push(tempVar);

           	tempVar.val = count++;
			tempVar.str = "VAR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("varDeclarations1")){
			tempVar.val = currStack.top();
			tempVar.str = "varDeclarations";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = currStack.top();
			tempVar.str = "varDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("varDeclarations2")){
        	tempVar.val = currStack.top();
			tempVar.str = "varDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("typeBody1")){
			tempVar.val = currStack.top();
			tempVar.str = "typeDeclarations";
			currStack.pop();
			tempStack.push(tempVar);

           	tempVar.val = count++;
			tempVar.str = "TYPE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("typeDeclarations1")){
			tempVar.val = currStack.top();
			tempVar.str = "typeDeclarations";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = currStack.top();
			tempVar.str = "typeDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("typeDeclarations2")){
        	tempVar.val = currStack.top();
			tempVar.str = "typeDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("constBody1")){
			tempVar.val = currStack.top();
			tempVar.str = "constDeclarations";
			currStack.pop();
			tempStack.push(tempVar);

           	tempVar.val = count++;
			tempVar.str = "CONST";
			tempStack.push(tempVar);
        }
        else if(!type.compare("constDeclarations1")){
			tempVar.val = currStack.top();
			tempVar.str = "constDeclarations";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = currStack.top();
			tempVar.str = "constDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("constDeclarations2")){
        	tempVar.val = currStack.top();
			tempVar.str = "constDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("integer1")){
        	tempVar.val = count++;
			tempVar.str = "INT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("integer2")){
        	tempVar.val = count++;
			tempVar.str = "OCTAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("integer3")){
        	tempVar.val = count++;
			tempVar.str = "HEXADECIMAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("integer4")){
        	tempVar.val = count++;
			tempVar.str = "BINARY";
			tempStack.push(tempVar);
        }
        else if(!type.compare("sign1")){
        	tempVar.val = count++;
			tempVar.str = "PLUS";
			tempStack.push(tempVar);
        }
        else if(!type.compare("sign2")){
        	tempVar.val = count++;
			tempVar.str = "MINUS";
			tempStack.push(tempVar);
        }
        else if(!type.compare("real1")){
        	tempVar.val = count++;
			tempVar.str = "FLOAT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("real2")){
        	tempVar.val = count++;
			tempVar.str = "EXPONENTIAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("number1")){
			tempVar.val = currStack.top();
			tempVar.str = "integer";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("number2")){
			tempVar.val = currStack.top();
			tempVar.str = "real";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("string1")){
        	tempVar.val = count++;
			tempVar.str = "CONST_CHAR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("string2")){
        	tempVar.val = count++;
			tempVar.str = "STR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("constDeclaration1")){
			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "EQUAL";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("typeDeclaration1")){
			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "EQUAL";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("type1")){
			tempVar.val = currStack.top();
			tempVar.str = "simpleType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("type2")){
			tempVar.val = currStack.top();
			tempVar.str = "stringType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("type3")){
			tempVar.val = currStack.top();
			tempVar.str = "structuredType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("type4")){
			tempVar.val = currStack.top();
			tempVar.str = "pointerType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("type5")){
			tempVar.val = currStack.top();
			tempVar.str = "proceduralType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("simpleType1")){
        	tempVar.val = currStack.top();
			tempVar.str = "ordinalType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("simpleType2")){
        	tempVar.val = currStack.top();
			tempVar.str = "realType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("simpleType3")){
        	tempVar.val = currStack.top();
			tempVar.str = "enumType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("simpleType4")){
        	tempVar.val = currStack.top();
			tempVar.str = "range";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalType1")){
        	tempVar.val = count++;
			tempVar.str = "CHAR";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType2")){
        	tempVar.val = count++;
			tempVar.str = "INTEGER";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType3")){
        	tempVar.val = count++;
			tempVar.str = "CARDINAL";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType4")){
        	tempVar.val = count++;
			tempVar.str = "SHORTINT";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType5")){
        	tempVar.val = count++;
			tempVar.str = "LONGINT";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType6")){
        	tempVar.val = count++;
			tempVar.str = "INT64";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType7")){
        	tempVar.val = count++;
			tempVar.str = "BYTE";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType8")){
        	tempVar.val = count++;
			tempVar.str = "WORD";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType9")){
        	tempVar.val = count++;
			tempVar.str = "LONGWORD";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType10")){
        	tempVar.val = count++;
			tempVar.str = "BOOLEAN";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalType11")){
        	tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
		}
        else if(!type.compare("ordinalTypeWithoutIdentifier1")){
        	tempVar.val = count++;
			tempVar.str = "CHAR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalTypeWithoutIdentifier2")){
        	tempVar.val = count++;
			tempVar.str = "INTEGER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalTypeWithoutIdentifier3")){
        	tempVar.val = count++;
			tempVar.str = "CARDINAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalTypeWithoutIdentifier4")){
        	tempVar.val = count++;
			tempVar.str = "SHORTINT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalTypeWithoutIdentifier5")){
        	tempVar.val = count++;
			tempVar.str = "LONGINT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalTypeWithoutIdentifier6")){
        	tempVar.val = count++;
			tempVar.str = "INT64";
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalTypeWithoutIdentifier7")){
        	tempVar.val = count++;
			tempVar.str = "BYTE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalTypeWithoutIdentifier8")){
        	tempVar.val = count++;
			tempVar.str = "WORD";
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalTypeWithoutIdentifier9")){
        	tempVar.val = count++;
			tempVar.str = "LONGWORD";
			tempStack.push(tempVar);
        }
        else if(!type.compare("ordinalTypeWithoutIdentifier10")){
        	tempVar.val = count++;
			tempVar.str = "BOOLEAN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("enumType1")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "enumList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("enumList1")){
        	tempVar.val = currStack.top();
			tempVar.str = "enumList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "enumStmt";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("enumList2")){
        	tempVar.val = currStack.top();
			tempVar.str = "enumStmt";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("enumStmt1")){
			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("enumStmt2")){
		    tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "CE";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("range1")){
			tempVar.val = currStack.top();
			tempVar.str = "integer";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOTDOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "integer";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("range2")){
			tempVar.val = currStack.top();
			tempVar.str = "integer";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOTDOT";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
		}
        else if(!type.compare("range3")){
			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOTDOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "integer";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("range4")){
        	tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOTDOT";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
		}
        else if(!type.compare("realType1")){
        	tempVar.val = count++;
			tempVar.str = "EXTENDED";
			tempStack.push(tempVar);
        }
        else if(!type.compare("realType2")){
        	tempVar.val = count++;
			tempVar.str = "REAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("stringType1")){
			tempVar.val = currStack.top();
			tempVar.str = "limit";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "STRING";
			tempStack.push(tempVar);
        }
        else if(!type.compare("limit1")){
        	tempVar.val = count++;
			tempVar.str = "RBRACKET";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LBRACKET";
			tempStack.push(tempVar);
        }
        else if(!type.compare("structuredType1")){
			tempVar.val = currStack.top();
			tempVar.str = "arrayType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("structuredType2")){
			tempVar.val = currStack.top();
			tempVar.str = "recordType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("structuredType3")){
			tempVar.val = currStack.top();
			tempVar.str = "setType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("structuredType4")){
			tempVar.val = currStack.top();
			tempVar.str = "fileType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("structuredType5")){
			tempVar.val = currStack.top();
			tempVar.str = "objectType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("arrayType1")){
			tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "ARRAY";
			tempStack.push(tempVar);
        }

        else if(!type.compare("arrayType2")){
			tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "RBRACKET";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "rangeList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LBRACKET";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "ARRAY";
			tempStack.push(tempVar);
        }
        else if(!type.compare("rangeList1")){
			tempVar.val = currStack.top();
			tempVar.str = "rangeList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "rangeWithIdentfier";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("rangeList2")){
        	tempVar.val = currStack.top();
			tempVar.str = "rangeWithIdentfier";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("rangeWithIdentfier1")){
			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOTDOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("rangeWithIdentfier2")){
			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("recordType1")){
        	tempVar.val = count++;
			tempVar.str = "END";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "RECORD";
			tempStack.push(tempVar);
        }
        else if(!type.compare("recordType2")){
        	tempVar.val = count++;
			tempVar.str = "END";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fieldList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "RECORD";
			tempStack.push(tempVar);
        }
        else if(!type.compare("fieldList1")){
			tempVar.val = currStack.top();
			tempVar.str = "fixedFields";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("fieldList2")){
        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fixedFields";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("fieldList3")){
			tempVar.val = currStack.top();
			tempVar.str = "variantPart";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("fieldList4")){
        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "variantPart";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("fieldList5")){
			tempVar.val = currStack.top();
			tempVar.str = "variantPart";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fixedFields";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("fieldList6")){
        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "variantPart";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fixedFields";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("fixedFields1")){
			tempVar.val = currStack.top();
			tempVar.str = "fixedFields";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("fixedFields2")){
			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("variantPart1")){
			tempVar.val = currStack.top();
			tempVar.str = "variantList";
			currStack.pop();
			tempStack.push(tempVar);

           	tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "ordinalType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

   			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

		   	tempVar.val = count++;
			tempVar.str = "CASE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("variantPart2")){
			tempVar.val = currStack.top();
			tempVar.str = "variantList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

   			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

		   	tempVar.val = count++;
			tempVar.str = "CASE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("variantPart1")){
			tempVar.val = currStack.top();
			tempVar.str = "variantList";
			currStack.pop();
			tempStack.push(tempVar);

           	tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "ordinalTypeWithoutIdentifier";
			currStack.pop();
			tempStack.push(tempVar);

		   	tempVar.val = count++;
			tempVar.str = "CASE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("variantList1")){
        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "variant";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("variantList2")){
        	tempVar.val = currStack.top();
			tempVar.str = "variantList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "variant";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("variant1")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "caseCondList";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("variant2")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fieldList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "caseCondList";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("fileType1")){
			tempVar.val = count++;
			tempVar.str = "FILEP";
			tempStack.push(tempVar);
        }
        else if(!type.compare("fileType2")){
        	tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "FILEP";
			tempStack.push(tempVar);
        }
        else if(!type.compare("pointerType1")){
        	tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "CAP";
			tempStack.push(tempVar);
		}
        else if(!type.compare("proceduralType1")){
			tempVar.val = currStack.top();
			tempVar.str = "procOrFuncTypeHeader";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("procOrFuncTypeHeader1")){
			tempVar.val = currStack.top();
			tempVar.str = "functionTypeHeader";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("procOrFuncTypeHeader2")){
			tempVar.val = currStack.top();
			tempVar.str = "procedureTypeHeader";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("functionTypeHeader1")){
			tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "FUNCTION";
			tempStack.push(tempVar);
        }
        else if(!type.compare("procedureTypeHeader1")){
			tempVar.val = currStack.top();
			tempVar.str = "parameterList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "PROCEDURE";
			tempStack.push(tempVar);
		}
        else if(!type.compare("varDeclaration1")){
        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("expr1")){
			tempVar.val = currStack.top();
			tempVar.str = "simpleExpr";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("expr2")){
			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "op1";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "simpleExpr";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("simpleExpr1")){
			tempVar.val = currStack.top();
			tempVar.str = "term";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("simpleExpr2")){
			tempVar.val = currStack.top();
			tempVar.str = "simpleExpr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "addOp";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "term";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("term1")){
			tempVar.val = currStack.top();
			tempVar.str = "factor";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("term2")){
			tempVar.val = currStack.top();
			tempVar.str = "term";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "mulOp2";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "factor";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("factor1")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor2")){
			tempVar.val = currStack.top();
			tempVar.str = "functionCall";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor3")){
			tempVar.val = currStack.top();
			tempVar.str = "varReference";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor4")){
			tempVar.val = currStack.top();
			tempVar.str = "unsignedConstant";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor5")){
			tempVar.val = currStack.top();
			tempVar.str = "factor";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "NOT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor6")){
			tempVar.val = currStack.top();
			tempVar.str = "factor";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "sign";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor7")){
			tempVar.val = currStack.top();
			tempVar.str = "setConstructor";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor8")){
			tempVar.val = currStack.top();
			tempVar.str = "valueTypecast";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor9")){
			tempVar.val = currStack.top();
			tempVar.str = "addressFactor";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor10")){
        	tempVar.val = count++;
			tempVar.str = "TRUE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("factor11")){
        	tempVar.val = count++;
			tempVar.str = "FALSE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("unsignedConstant1")){
        	tempVar.val = currStack.top();
			tempVar.str = "number";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("unsignedConstant2")){
        	tempVar.val = currStack.top();
			tempVar.str = "string";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("unsignedConstant3")){
        	tempVar.val = count++;
			tempVar.str = "NIL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("functionCall1")){
        	tempVar.val = currStack.top();
			tempVar.str = "actualParameterList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "functionCallPart1";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("actualParameterList1")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("actualParameterList2")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "exprList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("exprList1")){
        	tempVar.val = currStack.top();
			tempVar.str = "exprList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("exprList2")){
			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("functionCallPart1-1")){
        	tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fieldReference";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("functionCallPart1-2")){
        	tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
		}
        else if(!type.compare("setConstructor1")){
        	tempVar.val = count++;
			tempVar.str = "RBRACKET";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LBRACKET";
			tempStack.push(tempVar);
        }
        else if(!type.compare("setConstructor2")){
        	tempVar.val = count++;
			tempVar.str = "RBRACKET";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "setGroupList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LBRACKET";
			tempStack.push(tempVar);
        }
        else if(!type.compare("setGroupList1")){
			tempVar.val = currStack.top();
			tempVar.str = "setGroupList";
			currStack.pop();
			tempStack.push(tempVar);

           	tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "setGroup";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("setGroupList2")){
			tempVar.val = currStack.top();
			tempVar.str = "setGroup";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("setGroup1")){
			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("setGroup2")){
        	tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOTDOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("valueTypecast1")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "ordinalTypeWithoutIdentifier";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("addressFactor1")){
			tempVar.val = currStack.top();
			tempVar.str = "varReference";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "AT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("statement1")){
			tempVar.val = currStack.top();
			tempVar.str = "openStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("statement2")){
			tempVar.val = currStack.top();
			tempVar.str = "closedStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("statement3")){
			tempVar.val = currStack.top();
			tempVar.str = "closedIfStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("notIfStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "compoundStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("notIfStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "caseStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("notIfStatement3")){
			tempVar.val = currStack.top();
			tempVar.str = "repetitiveStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("notIfStatement4")){
			tempVar.val = currStack.top();
			tempVar.str = "withStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("notIfStatement5")){
			tempVar.val = currStack.top();
			tempVar.str = "assignmtStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("notIfStatement6")){
			tempVar.val = currStack.top();
			tempVar.str = "procStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "compoundStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "caseStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement3")){
			tempVar.val = currStack.top();
			tempVar.str = "closedForStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement4")){
			tempVar.val = currStack.top();
			tempVar.str = "repeatStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement5")){
			tempVar.val = currStack.top();
			tempVar.str = "closedWhileStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement6")){
			tempVar.val = currStack.top();
			tempVar.str = "closedWithStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement7")){
			tempVar.val = currStack.top();
			tempVar.str = "assignmtStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement8")){
			tempVar.val = currStack.top();
			tempVar.str = "procStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement9")){
			tempVar.val = currStack.top();
			tempVar.str = "closedIfStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedStatement10")){
			tempVar.val = count++;
			tempVar.str = "EXIT";
			tempStack.push(tempVar);
		}
        else if(!type.compare("closedStatement11")){
			tempVar.val = count++;
			tempVar.str = "BREAK";
			tempStack.push(tempVar);
		}
        else if(!type.compare("closedStatement12")){
			tempVar.val = count++;
			tempVar.str = "CONTINUE";
			tempStack.push(tempVar);
		}
        else if(!type.compare("openStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "openIfStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("openStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "openWithStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("openStatement3")){
			tempVar.val = currStack.top();
			tempVar.str = "openWhileStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("openStatement4")){
			tempVar.val = currStack.top();
			tempVar.str = "openForStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("openIfStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "statement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "THEN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IF";
			tempStack.push(tempVar);
        }
        else if(!type.compare("openIfStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "openStatement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "ELSE";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "closedStatement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "THEN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IF";
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedIfStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "closedStatement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "ELSE";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "closedStatement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "THEN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IF";
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedIfStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "statement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "ELSE";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "closedIfStatement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "THEN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IF";
			tempStack.push(tempVar);
        }
        else if(!type.compare("assignmtStatement1")){
        	tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "CE";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "varReference";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("procStatement1")){
        	tempVar.val = currStack.top();
			tempVar.str = "functionCall";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("procStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "actualParameterList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "READ";
			tempStack.push(tempVar);
        }
        else if(!type.compare("procStatement3")){
			tempVar.val = currStack.top();
			tempVar.str = "actualParameterList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "WRITE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("procStatement4")){
			tempVar.val = currStack.top();
			tempVar.str = "allocParameterList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "NEW";
			tempStack.push(tempVar);
        }
        else if(!type.compare("procStatement5")){
			tempVar.val = currStack.top();
			tempVar.str = "allocParameterList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DISPOSE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("allocParameterList1")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "exprList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("allocParameterList2")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("repetitiveStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "forStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("repetitiveStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "repeatStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("repetitiveStatement3")){
			tempVar.val = currStack.top();
			tempVar.str = "whileStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("compoundStatement1")){
        	tempVar.val = count++;
			tempVar.str = "END";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "statementList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "BEGINP";
			tempStack.push(tempVar);
        }
        else if(!type.compare("statementList1")){
			tempVar.val = currStack.top();
			tempVar.str = "statementList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "statement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("caseStatement1")){
        	tempVar.val = count++;
			tempVar.str = "END";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "caseList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "CASE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("caseStatement2")){
        	tempVar.val = count++;
			tempVar.str = "END";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "statementList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "otherwisePart";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "caseList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "CASE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("caseList1")){
			tempVar.val = currStack.top();
			tempVar.str = "caseList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "caseStmt";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("caseList2")){
        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "caseStmt";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("maybeElsePart1")){
			tempVar.val = currStack.top();
			tempVar.str = "elsePart";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("caseStmt1")){
			tempVar.val = currStack.top();
			tempVar.str = "statement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "caseCondList";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("caseCondList1")){
			tempVar.val = currStack.top();
			tempVar.str = "caseCondList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "caseCond";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("caseCondList2")){
			tempVar.val = currStack.top();
			tempVar.str = "caseCond";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("caseCond1")){
			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("caseCond2")){
			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOTDOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);
        } 
        else if(!type.compare("otherwisePart1")){
			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OTHERWISE";
			tempStack.push(tempVar);
        }         
        else if(!type.compare("compileTimeConstant1")){
			tempVar.val = currStack.top();
			tempVar.str = "enumType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("compileTimeConstant2")){
			tempVar.val = currStack.top();
			tempVar.str = "integer";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("compileTimeConstant3")){
        	tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
		}
        else if(!type.compare("compileTimeConstant4")){
			tempVar.val = currStack.top();
			tempVar.str = "string";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("compileTimeConstant5")){
        	tempVar.val = count++;
			tempVar.str = "TRUE";
			tempStack.push(tempVar);
		}
        else if(!type.compare("compileTimeConstant6")){
        	tempVar.val = count++;
			tempVar.str = "FALSE";
			tempStack.push(tempVar);
		}
        else if(!type.compare("elsePart1")){
        	tempVar.val = currStack.top();
			tempVar.str = "statementList";
			currStack.pop();
			tempStack.push(tempVar);

	        tempVar.val = count++;
			tempVar.str = "ELSE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("elsePart2")){
        	tempVar.val = currStack.top();
			tempVar.str = "statementList";
			currStack.pop();
			tempStack.push(tempVar);

	        tempVar.val = count++;
			tempVar.str = "OTHERWISE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("openForStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "openForToDowntoStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("openForStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "openForInStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedForStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "closedForToDowntoStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedForStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "closedForInStatement";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedForToDowntoStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "closedStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "finalValue";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "TO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "initialValue";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "CE";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "controlVariable";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "FOR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedForToDowntoStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "closedStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "finalValue";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DOWNTO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "initialValue";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "CE";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "controlVariable";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "FOR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("openForToDowntoStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "openStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "finalValue";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "TO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "initialValue";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "CE";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "controlVariable";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "FOR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("openForToDowntoStatement2")){
			tempVar.val = currStack.top();
			tempVar.str = "openStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "finalValue";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DOWNTO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "initialValue";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "CE";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "controlVariable";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "FOR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("controlVariable1")){
        	tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("initialValue1")){
			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("finalValue1")){
			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("openForInStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "openStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "enumerable";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "IN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "controlVariable";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "FOR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedForInStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "closedStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "enumerable";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "IN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "controlVariable";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "FOR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("enumerable1")){
			tempVar.val = currStack.top();
			tempVar.str = "enumType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("enumerable2")){
			tempVar.val = currStack.top();
			tempVar.str = "string";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("enumerable3")){
			tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "stringList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("stringList1")){
			tempVar.val = currStack.top();
			tempVar.str = "stringList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "string";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("stringList2")){
			tempVar.val = currStack.top();
			tempVar.str = "string";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("repeatStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "UNTIL";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "statementList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "REPEAT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("openWhileStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "openStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "WHILE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("closedWhileStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "closedStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "expr";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "WHILE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("openWithStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "openStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "varReferenceList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "WITH";
			tempStack.push(tempVar);
		}
        else if(!type.compare("closedWithStatement1")){
			tempVar.val = currStack.top();
			tempVar.str = "closedStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "DO";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "varReferenceList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "WITH";
			tempStack.push(tempVar);
		}
        else if(!type.compare("varReferenceList1")){
			tempVar.val = currStack.top();
			tempVar.str = "varReferenceList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "varReference";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("varReferenceList2")){
			tempVar.val = currStack.top();
			tempVar.str = "varReference";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("procDeclaration1")){
			tempVar.val = currStack.top();
			tempVar.str = "subroutineBlock";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "procedureHeader";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("procDeclaration2")){
			tempVar.val = currStack.top();
			tempVar.str = "subroutineBlockWithoutForward";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "PROCEDURE";
			tempStack.push(tempVar);
		}
        else if(!type.compare("subroutineBlockWithoutForward1")){
			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "compoundStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("subroutineBlockWithoutForward2")){
			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "externalDirective";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("procedureHeader1")){
        	tempVar.val = currStack.top();
			tempVar.str = "parameterList";
			currStack.pop();
			tempStack.push(tempVar);

       		tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "PROCEDURE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("subroutineBlock1")){
			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "compoundStatement";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("subroutineBlock2")){
			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "externalDirective";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("subroutineBlock3")){
			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "FORWARD";
			tempStack.push(tempVar);
		}
        else if(!type.compare("funcDeclaration1")){
			tempVar.val = currStack.top();
			tempVar.str = "subroutineBlock";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "functionHeader";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("funcDeclaration2")){
			tempVar.val = currStack.top();
			tempVar.str = "subroutineBlockWithoutForward";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "FUNCTION";
			tempStack.push(tempVar);
		}
        else if(!type.compare("functionHeader1")){
        	tempVar.val = currStack.top();
			tempVar.str = "type";
			currStack.pop();
			tempStack.push(tempVar);

       		tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

        	tempVar.val = currStack.top();
			tempVar.str = "parameterList";
			currStack.pop();
			tempStack.push(tempVar);

       		tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "FUNCTION";
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterList1")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterList2")){
        	tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterDeclarationList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterDeclarationList1")){
			tempVar.val = currStack.top();
			tempVar.str = "parameterDeclarationList";
			currStack.pop();
			tempStack.push(tempVar);

        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterDeclarationList2")){
			tempVar.val = currStack.top();
			tempVar.str = "parameterDeclaration";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterDeclaration1")){
			tempVar.val = currStack.top();
			tempVar.str = "valueParameter";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterDeclaration2")){
			tempVar.val = currStack.top();
			tempVar.str = "variableParameter";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterDeclaration3")){
			tempVar.val = currStack.top();
			tempVar.str = "outParameter";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterDeclaration4")){
			tempVar.val = currStack.top();
			tempVar.str = "constantParameter";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("valueParameter1")){
        	tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("valueParameter2")){
        	tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "ARRAY";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterType1")){
			tempVar.val = currStack.top();
			tempVar.str = "ordinalType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterType2")){
        	tempVar.val = count++;
			tempVar.str = "STRING";
			tempStack.push(tempVar);
        }
        else if(!type.compare("parameterType3")){
			tempVar.val = currStack.top();
			tempVar.str = "realType";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("variableParameter1")){
			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "VAR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("variableParameter2")){
			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "VAR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("variableParameter3")){
			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "ARRAY";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "VAR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("outParameter1")){
			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OUT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("outParameter2")){
			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OUT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("outParameter3")){
			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "ARRAY";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OUT";
			tempStack.push(tempVar);
        }
        else if(!type.compare("constantParameter1")){
			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "CONST";
			tempStack.push(tempVar);
        }
        else if(!type.compare("constantParameter2")){
			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "CONST";
			tempStack.push(tempVar);
        }
        else if(!type.compare("constantParameter3")){
			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OF";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "ARRAY";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "identifierList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "CONST";
			tempStack.push(tempVar);
        }
        else if(!type.compare("externalDirective1")){
			tempVar.val = count++;
			tempVar.str = "EXTERNAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("externalDirective2")){
			tempVar.val = currStack.top();
			tempVar.str = "string";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "EXTERNAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("externalDirective3")){
			tempVar.val = currStack.top();
			tempVar.str = "string";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "NAME";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "string";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "EXTERNAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("externalDirective4")){
			tempVar.val = currStack.top();
			tempVar.str = "number";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "INDEX";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "string";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "EXTERNAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("varReference1")){
       		tempVar.val = currStack.top();
			tempVar.str = "nonPointerReference";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("varReference2")){
       		tempVar.val = currStack.top();
			tempVar.str = "pointerReference";
			currStack.pop();
			tempStack.push(tempVar);
        }  
        else if(!type.compare("nonPointerReference1")){
       		tempVar.val = currStack.top();
			tempVar.str = "indexedReference";
			currStack.pop();
			tempStack.push(tempVar);
        } 
        else if(!type.compare("nonPointerReference2")){
       		tempVar.val = currStack.top();
			tempVar.str = "pointerReference";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fieldReference";
			currStack.pop();
			tempStack.push(tempVar);
        }  
        else if(!type.compare("pointerReference1")){
			tempVar.val = count++;
			tempVar.str = "CAP";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "pointerReference";
			currStack.pop();
			tempStack.push(tempVar);
        } 
        else if(!type.compare("pointerReference2")){
			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }     
        else if(!type.compare("indexedReference4")){
        	tempVar.val = currStack.top();
			tempVar.str = "fieldReference";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "arrayReference";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fieldReference";
			currStack.pop();
			tempStack.push(tempVar);
        }     
        else if(!type.compare("indexedReference1")){
			tempVar.val = currStack.top();
			tempVar.str = "arrayReference";
			currStack.pop();
			tempStack.push(tempVar);
        }       
        else if(!type.compare("indexedReference2")){
        	tempVar.val = currStack.top();
			tempVar.str = "fieldReference";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "arrayReference";
			currStack.pop();
			tempStack.push(tempVar);
        }      
        else if(!type.compare("indexedReference3")){
			tempVar.val = currStack.top();
			tempVar.str = "arrayReference";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fieldReference";
			currStack.pop();
			tempStack.push(tempVar);
        }    
        else if(!type.compare("indexedReference4")){
			tempVar.val = currStack.top();
			tempVar.str = "fieldReference";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "arrayReference";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fieldReference";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("arrayReference1")){
        	tempVar.val = count++;
			tempVar.str = "RBRACKET";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "exprList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LBRACKET";
			tempStack.push(tempVar);
			
			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("fieldReference1")){
        	tempVar.val = currStack.top();
			tempVar.str = "pointerReference";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "DOT";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "fieldReference";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("fieldReference2")){
        	tempVar.val = currStack.top();
			tempVar.str = "pointerReference";
			currStack.pop();
			tempStack.push(tempVar);
        }
        else if(!type.compare("operatorDeclaration1")){
        	tempVar.val = currStack.top();
			tempVar.str = "assignmtDefinition";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OPERATOR";
			tempStack.push(tempVar);
		}
        else if(!type.compare("operatorDeclaration2")){
        	tempVar.val = currStack.top();
			tempVar.str = "arithmeticDefinition";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OPERATOR";
			tempStack.push(tempVar);
		}
        else if(!type.compare("operatorDeclaration3")){
        	tempVar.val = currStack.top();
			tempVar.str = "comparisonDefinition";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "OPERATOR";
			tempStack.push(tempVar);
		}
        else if(!type.compare("assignmtDefinition1")){
        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "compoundStatement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "CE";
			tempStack.push(tempVar);
		}
        else if(!type.compare("arithmeticDefinition1")){
        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "compoundStatement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterList2";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "op2";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("comparisonDefinition1")){
        	tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "compoundStatement";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "initList";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "BOOLEAN";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "RPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterList2";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "LPAREN";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "compOp";
			currStack.pop();
			tempStack.push(tempVar);
		}
        else if(!type.compare("parameterList2-1")){
        	tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COMMA";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
		}
        else if(!type.compare("parameterList2-2")){
        	tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "SEMICOLON";
			tempStack.push(tempVar);

			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
		}	
        else if(!type.compare("parameterList2-3")){
   			tempVar.val = currStack.top();
			tempVar.str = "parameterType";
			currStack.pop();
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "COLON";
			tempStack.push(tempVar);

			tempVar.val = count++;
			tempVar.str = "IDENTIFIER";
			tempStack.push(tempVar);
		}	
		else if(!type.compare("objectType1")){
            tempVar.val = count++;
            tempVar.str = "END";
            tempStack.push(tempVar);
 
            tempVar.val = currStack.top();
                        tempVar.str = "componentList";
                        currStack.pop();
                        tempStack.push(tempVar);
 
            tempVar.val = count++;
                        tempVar.str = "OBJECT";
                        tempStack.push(tempVar);
        }
        else if(!type.compare("componentList1")){
 
            tempVar.val = currStack.top();
            tempVar.str = "componentList";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = currStack.top();
            tempVar.str = "component";
            currStack.pop();
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("componentList2")){
            tempVar.val = currStack.top();
            tempVar.str = "component";
            currStack.pop();
            tempStack.push(tempVar);
        }
        else if(!type.compare("component1")){
 
            tempVar.val = currStack.top();
            tempVar.str = "fieldOrMethodDefinitionList";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = currStack.top();
            tempVar.str = "visibilitySpecifier";
            currStack.pop();
            tempStack.push(tempVar);
        }
        else if(!type.compare("fieldOrMethodDefinitionList1")){
 
            tempVar.val = currStack.top();
            tempVar.str = "fieldOrMethodDefinitionList";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = currStack.top();
            tempVar.str = "fieldOrMethodDefinition";
            currStack.pop();
            tempStack.push(tempVar);
        }
        else if(!type.compare("fieldOrMethodDefinitionList2")){
            tempVar.val = currStack.top();
            tempVar.str = "fieldOrMethodDefinition";
            currStack.pop();
            tempStack.push(tempVar);
        }
        else if(!type.compare("fieldOrMethodDefinition1")){
 
            tempVar.val = currStack.top();
            tempVar.str = "fieldDefinition";
            currStack.pop();
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("fieldOrMethodDefinition2")){
           
            tempVar.val = currStack.top();
            tempVar.str = "methodDefinition";
            currStack.pop();
            tempStack.push(tempVar);
        }
        else if(!type.compare("fieldDefinition1")){
            tempVar.val = count++;
            tempVar.str = "SEMICOLON";
            tempStack.push(tempVar);
 
            tempVar.val = currStack.top();
            tempVar.str = "type";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = count++;
            tempVar.str = "COLON";
            tempStack.push(tempVar);
 
            tempVar.val = currStack.top();
            tempVar.str = "identifierList";
            currStack.pop();
            tempStack.push(tempVar);
        }
        else if(!type.compare("constructorDeclaration1")){
 
            tempVar.val = currStack.top();
            tempVar.str = "subroutineBlock";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = count++;
            tempVar.str = "SEMICOLON";
            tempStack.push(tempVar);
 
            tempVar.val = currStack.top();
            tempVar.str = "parameterList";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = count++;
            tempVar.str = "IDENTIFIER";
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "DOT";
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "IDENTIFIER";
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "CONSTRUCTOR";
            tempStack.push(tempVar);
        }
        else if(!type.compare("destructorDeclaration1")){
 
            tempVar.val = currStack.top();
            tempVar.str = "subroutineBlock";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = count++;
            tempVar.str = "SEMICOLON";
            tempStack.push(tempVar);
 
            tempVar.val = currStack.top();
            tempVar.str = "parameterList";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = count++;
            tempVar.str = "IDENTIFIER";
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "DOT";
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "IDENTIFIER";
            tempStack.push(tempVar);

            tempVar.val = count++;
            tempVar.str = "DESTRUCTOR";
            tempStack.push(tempVar);
        }
        else if(!type.compare("constructorHeader1")){
        	tempVar.val = currStack.top();
            tempVar.str = "parameterList";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = count++;
            tempVar.str = "IDENTIFIER";
            tempStack.push(tempVar);
 
            tempVar.val = count++;
            tempVar.str = "CONSTRUCTOR";
            tempStack.push(tempVar);
        }
        else if(!type.compare("destructorHeader1")){
        	tempVar.val = currStack.top();
            tempVar.str = "parameterList";
            currStack.pop();
            tempStack.push(tempVar);
 
            tempVar.val = count++;
            tempVar.str = "IDENTIFIER";
            tempStack.push(tempVar);
 
            tempVar.val = count++;
            tempVar.str = "DESTRUCTOR";
            tempStack.push(tempVar);
        }
        else if(!type.compare("methodDefinition1")){
            tempVar.val = count++;
            tempVar.str = "SEMICOLON";
            tempStack.push(tempVar);
 
            tempVar.val = currStack.top();
            tempVar.str = "methodDefinitionHeader";
            currStack.pop();
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("methodDefinitionHeader1")){
 
            tempVar.val = currStack.top();
            tempVar.str = "functionHeader";
            currStack.pop();
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("methodDefinitionHeader2")){
 
            tempVar.val = currStack.top();
            tempVar.str = "procedureHeader";
            currStack.pop();
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("methodDefinitionHeader3")){
 
            tempVar.val = currStack.top();
            tempVar.str = "constructorHeader";
            currStack.pop();
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("methodDefinitionHeader4")){
 
            tempVar.val = currStack.top();
            tempVar.str = "destructorHeader";
            currStack.pop();
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("visibilitySpecifier1")){
            tempVar.val = count++;
            tempVar.str = "PRIVATE";
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("visibilitySpecifier2")){
            tempVar.val = count++;
            tempVar.str = "PROTECTED";
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("visibilitySpecifier3")){
            tempVar.val = count++;
            tempVar.str = "PUBLIC";
            tempStack.push(tempVar);
 
        }
        else if(!type.compare("compOp1")){
            tempVar.val = count++;
            tempVar.str = "EQUAL";
            tempStack.push(tempVar);
        }
        else if(!type.compare("compOp2")){
            tempVar.val = count++;
            tempVar.str = "LESS";
            tempStack.push(tempVar);
        }
        else if(!type.compare("compOp3")){
            tempVar.val = count++;
            tempVar.str = "GREATER";
            tempStack.push(tempVar);
        }
        else if(!type.compare("compOp4")){
            tempVar.val = count++;
            tempVar.str = "LE";
            tempStack.push(tempVar);
        }
        else if(!type.compare("compOp5")){
            tempVar.val = count++;
            tempVar.str = "GE";
            tempStack.push(tempVar);
        }
        else if(!type.compare("compOp6")){
            tempVar.val = count++;
            tempVar.str = "IN";
            tempStack.push(tempVar);
        }
        else if(!type.compare("op2-1")){
            tempVar.val = count++;
            tempVar.str = "PLUS";
            tempStack.push(tempVar);
        }
        else if(!type.compare("op2-2")){
            tempVar.val = count++;
            tempVar.str = "MINUS";
            tempStack.push(tempVar);
        }
        else if(!type.compare("op2-3")){
            tempVar.val = count++;
            tempVar.str = "MUL";
            tempStack.push(tempVar);
        }
        else if(!type.compare("op2-4")){
            tempVar.val = count++;
            tempVar.str = "DIVIDE";
            tempStack.push(tempVar);
        }
        else if(!type.compare("op2-5")){
            tempVar.val = count++;
            tempVar.str = "SS";
            tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-9")){
            tempVar.val = count++;
            tempVar.str = "LL";
            tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-10")){
            tempVar.val = count++;
            tempVar.str = "GG";
            tempStack.push(tempVar);
        }
        else if(!type.compare("op1-8")){
            tempVar.val = count++;
            tempVar.str = "LESS";
            tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-1")){
        	tempVar.val = count++;
			tempVar.str = "MUL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-2")){
        	tempVar.val = count++;
			tempVar.str = "DIVIDE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-3")){
        	tempVar.val = count++;
			tempVar.str = "DIV";
			tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-4")){
        	tempVar.val = count++;
			tempVar.str = "MOD";
			tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-5")){
        	tempVar.val = count++;
			tempVar.str = "AND";
			tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-6")){
        	tempVar.val = count++;
			tempVar.str = "AS";
			tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-7")){
        	tempVar.val = count++;
			tempVar.str = "SHL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("mulOp2-8")){
        	tempVar.val = count++;
			tempVar.str = "SHR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("op1-1")){
        	tempVar.val = count++;
			tempVar.str = "LE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("op1-2")){
        	tempVar.val = count++;
			tempVar.str = "GREATER";
			tempStack.push(tempVar);
        }
        else if(!type.compare("op1-3")){
        	tempVar.val = count++;
			tempVar.str = "GE";
			tempStack.push(tempVar);
        }
        else if(!type.compare("op1-4")){
        	tempVar.val = count++;
			tempVar.str = "EQUAL";
			tempStack.push(tempVar);
        }
        else if(!type.compare("op1-5")){
        	tempVar.val = count++;
			tempVar.str = "LG";
			tempStack.push(tempVar);
        }
        else if(!type.compare("op1-6")){
        	tempVar.val = count++;
			tempVar.str = "IN";
			tempStack.push(tempVar);
        }
        else if(!type.compare("op1-7")){
        	tempVar.val = count++;
			tempVar.str = "IS";
			tempStack.push(tempVar);
        }
        else if(!type.compare("addOp1")){
        	tempVar.val = count++;
			tempVar.str = "PLUS";
			tempStack.push(tempVar);
        }
        else if(!type.compare("addOp2")){
        	tempVar.val = count++;
			tempVar.str = "MINUS";
			tempStack.push(tempVar);
        }
        else if(!type.compare("addOp3")){
        	tempVar.val = count++;
			tempVar.str = "OR";
			tempStack.push(tempVar);
        }
        else if(!type.compare("addOp4")){
        	tempVar.val = count++;
			tempVar.str = "XOR";
			tempStack.push(tempVar);
        }
        else{
            perror("Unkown type in toWrite().\n");
            return 0;
        }
        ostringstream toReturn;
        for(    ; !tempStack.empty(); tempStack.pop()){
            toReturn << count << " -> { " << tempStack.top().val << "[label=\"" << tempStack.top().str << "\"] }\n";
            toReturn << "//current count=" << count << " with type=\"" << type << "\".\n";
        }
    //    printStackContent();
        currStack.push(count++);
        return toReturn.str();
    }
%}

%token  COPERATORS  DEFINEC  DEFINE  ELSEC  ELSEIF ELIFC  ENDC  ENDIF  ERRORC  ERROR  FATAL  HPLUS  HMINUS  LONGSTRINGS  IFNDEF  IFDEF  IFOPT  IFC  IF  INCLUDEPATH  INFO  INLINE  IPLUS  IMINUS  I  LINKLIB  LINK  L  MACRO  MESSAGE  STATIC  STOP  UNDEFC  UNDEF  WARNINGS  WARNING  WARN  OBJECTPATH LEFTCURLY  RIGHTCURLY  LEFTBSTAR  RIGHTBSTAR COMMENT END_OF_PROGRAM  CHAR  ABSOLUTE  ARRAY  ASM  BEGINP  CASE  CONST  CONSTRUCTOR  DESTRUCTOR  DO  DOWNTO  END  FILEP  FOR  FUNCTION  GOTO  IMPLEMENTATION  INHERITED  INTERFACE  LABEL  NIL  OBJECT  OF  OPERATOR PROCEDURE  PROGRAM  RECORD  REINTRODUCE  REPEAT  SELF  SET STRING  TO  TYPE  UNIT  UNTIL  USES  VAR  WHILE  WITH  DISPOSE  FALSE  TRUE  EXIT  NEW  AS  CLASS  DISPINTERFACE  EXCEPT  EXPORTS  FINALIZATION  FINALLY  INTIALIZATION  IS  LIBRARY  ON  OUT  BITPACKED  PROPERLY  RAISE  RESOURCESTRING  THREADVAR  TRY  ABSTRACT  ALIAS  ASSEMBLER  PACKED  BREAK  CDECL  CONTINUE  CPPDECL  CVAR  DEFAULT  DEPRECATED  DYNAMIC  ENUMERATOR  EXPERIMENTAL  EXPORT  EXTERNAL  FAR  FAR16  FORWARD  GENERIC  HELPER  IMPLEMENTS  INDEX  INTERRUPT  IOCHECKS  LOCAL  NAME  NEAR  NODEFAULT  NORETURN  NOSTACKFRAME  OLDFPCCALL  OTHERWISE  OVERLOAD  OVERRIDE  PASCAL  PLATFORM  PRIVATE  PROTECTED  PUBLIC  PUBLISHED  READ  REGISTER  RESULT  SAFECALL  SAVEREGISTERS  SOFTFLOAT  SPECIALIZE  STDCALL  STORED  STRICT  UNALLIGNED  UNIMPLEMENTED  VARARGS  VIRTUAL  WRITE  INTEGER  CARDINAL  SHORTINT  SMALLINT  LONGINT  INT64  BYTE  LONGWORD  WORD  EXTENDED  BOOLEAN  REAL NESTED IDENTIFIER NEWLINE  SPACE BADIDENTIFIER  ALIEN THEN ELSE

%left EQUAL LG LESS GREATER LE GE IN
%left PLUS MINUS OR XOR
%left DIVIDE MUL DIV MOD AND
%left NOT

%token	CE PL  ML  PG  MG  SL  DL  SG  DG COLON  SEMICOLON  COMMA  LPAREN  RPAREN  LBRACKET  RBRACKET  CAP  DOTDOT  DOT  AT LL  GG  SHL  SHR INCLUDE SS

%token <intVal> HEXADECIMAL
%token <intVal> OCTAL
%token <intVal> BINARY
%token <intVal> INT
%token <floatVal> EXPONENTIAL
%token <floatVal> FLOAT
%token <strVal> STR
%token <strval> CONST_CHAR

%union {
    char* strVal;
    long long int intVal;
    double floatVal;
}
%start file

%%
file:
    PROGRAM IDENTIFIER cmdArguments SEMICOLON initList BEGINP statementList END_OF_PROGRAM {outFile << toWrite("file");}
    ;
cmdArguments:
	LPAREN identifierList RPAREN {outFile << toWrite("cmdArguments1");}
	|
	/* epsilon*/	{outFile << toWrite("epsilon");}
	;
identifierList:    //cannot be empty
	IDENTIFIER COMMA identifierList  {outFile << toWrite("identifierList1");}
	|
	IDENTIFIER      {outFile << toWrite("identifierList2");}
	;
initList:
	varBody	initList {outFile << toWrite("initList1");}
	|
	typeBody initList 	{outFile << toWrite("initList2");}
	|
	constBody initList 	{outFile << toWrite("initList3");}
	|
	procDeclaration initList 	{outFile << toWrite("initList4");}
	|
	funcDeclaration initList 	{outFile << toWrite("initList5");}
	|
	operatorDeclaration initList 	{outFile << toWrite("initList6");}
	|
	constructorDeclaration initList 	{outFile << toWrite("initList7");}
	|
	destructorDeclaration initList 	{outFile << toWrite("initList8");}
	|
	/* epsilon */  {outFile << toWrite("epsilon");}
	;
varBody:
	VAR varDeclarations		{outFile << toWrite("varBody1");}
	;
varDeclarations:
	varDeclaration varDeclarations 	{outFile << toWrite("varDeclarations1");}
	|
	varDeclaration 		{outFile << toWrite("varDeclarations2");}
	;
typeBody:
	TYPE typeDeclarations 	{outFile << toWrite("typeBody1");}
	;
typeDeclarations:
	typeDeclaration typeDeclarations 	{outFile << toWrite("typeDeclarations1");}
	|
	typeDeclaration 	{outFile << toWrite("typeDeclarations2");}
	;
constBody:
	CONST constDeclarations 	{outFile << toWrite("constBody1");}
	;
constDeclarations:
	constDeclaration constDeclarations 	{outFile << toWrite("constDeclarations1");}
	|
	constDeclaration 	{outFile << toWrite("constDeclarations2");}
	;
integer:
	INT 	{outFile << toWrite("integer1");}
	|
	OCTAL 	{outFile << toWrite("integer2");}
	|
	HEXADECIMAL 	{outFile << toWrite("integer3");}
	|
	BINARY 		{outFile << toWrite("integer4");}
	;
sign:
	PLUS 	{outFile << toWrite("sign1");}
	|
	MINUS 	{outFile << toWrite("sign2");}
	;
real:
	FLOAT 	{outFile << toWrite("real1");}
	|
	EXPONENTIAL 	{outFile << toWrite("real2");}
	;
number:
	integer 	{outFile << toWrite("number1");}
	|
	real 	{outFile << toWrite("number2");}
	;
string:
	CONST_CHAR 		{outFile << toWrite("string1");}
	|
	STR 	{outFile << toWrite("string2");}
	;
constDeclaration:
	IDENTIFIER EQUAL expr SEMICOLON 	{outFile << toWrite("constDeclaration1");} /*check for strings in expr*/
	;
typeDeclaration:
	IDENTIFIER EQUAL type SEMICOLON 	{outFile << toWrite("typeDeclaration1");}
	;
type:  
	simpleType 		{outFile << toWrite("type1");}
	|
	stringType 		{outFile << toWrite("type2");}
	|
	structuredType 	{outFile << toWrite("type3");}
	|
	pointerType		{outFile << toWrite("type4");}
	|
	proceduralType 	{outFile << toWrite("type5");}
	;
simpleType:
	ordinalType 	{outFile << toWrite("simpleType1");}
	|
	realType 	{outFile << toWrite("simpleType2");}
	|
	enumType 	{outFile << toWrite("simpleType3");}
	|
	range 	{outFile << toWrite("simpleType4");}
	;
ordinalType:
	CHAR 	{outFile << toWrite("ordinalType1");}
	|
	INTEGER 	{outFile << toWrite("ordinalType2");}
	|
	CARDINAL 	{outFile << toWrite("ordinalType3");}
	|
	SHORTINT 	{outFile << toWrite("ordinalType4");}
	|
	LONGINT 	{outFile << toWrite("ordinalType5");}
	|
	INT64 		{outFile << toWrite("ordinalType6");}
	|
	BYTE 		{outFile << toWrite("ordinalType7");}
	|
	WORD 		{outFile << toWrite("ordinalType8");}
	|
	LONGWORD 	{outFile << toWrite("ordinalType9");}
	|
	BOOLEAN 	{outFile << toWrite("ordinalType10");}
	|
	IDENTIFIER 	{outFile << toWrite("ordinalType11");}
	;
ordinalTypeWithoutIdentifier:
	CHAR 	{outFile << toWrite("ordinalTypeWithoutIdentifier1");}
	|
	INTEGER 	{outFile << toWrite("ordinalTypeWithoutIdentifier2");}
	|
	CARDINAL 	{outFile << toWrite("ordinalTypeWithoutIdentifier3");}
	|
	SHORTINT 	{outFile << toWrite("ordinalTypeWithoutIdentifier4");}
	|
	LONGINT 	{outFile << toWrite("ordinalTypeWithoutIdentifier5");}
	|
	INT64 		{outFile << toWrite("ordinalTypeWithoutIdentifier6");}
	|
	BYTE 		{outFile << toWrite("ordinalTypeWithoutIdentifier7");}
	|
	WORD 		{outFile << toWrite("ordinalTypeWithoutIdentifier8");}
	|
	LONGWORD 	{outFile << toWrite("ordinalTypeWithoutIdentifier9");}
	|
	BOOLEAN 	{outFile << toWrite("ordinalTypeWithoutIdentifier10");}
	;
enumType:
	LPAREN enumList RPAREN	{outFile << toWrite("enumType1");}
	;
enumList:
	enumStmt COMMA enumList 	{outFile << toWrite("enumList1");}
	|
	enumStmt 	{outFile << toWrite("enumList2");}
	;
enumStmt:
	IDENTIFIER 	{outFile << toWrite("enumStmt1");} 	
	|
	IDENTIFIER CE expr 	{outFile << toWrite("enumStmt2");}
	;
range:
	integer DOTDOT integer 	{outFile << toWrite("range1");}
	|
	IDENTIFIER DOTDOT integer 	{outFile << toWrite("range2");}
	|
	integer DOTDOT IDENTIFIER 	{outFile << toWrite("range3");}
	|
	IDENTIFIER DOTDOT IDENTIFIER 	{outFile << toWrite("range4");}
	;
realType:
	EXTENDED 	{outFile << toWrite("realType1");}
	|
	REAL 		{outFile << toWrite("realType2");}
	;
stringType:
	STRING limit 	{outFile << toWrite("stringType1");}
	;
limit:
	/*epsilon*/ 	{outFile << toWrite("epsilon");}
	|
	LBRACKET expr RBRACKET 	{outFile << toWrite("limit1");} /*added by me it is LBRACKET integer RBRACKET in ref.pdf*/
	;
structuredType:
	arrayType 	{outFile << toWrite("structuredType1");}
	|
	recordType 	{outFile << toWrite("structuredType2");}
	|
	fileType 	{outFile << toWrite("structuredType4");}
	|
	objectType 	{outFile << toWrite("structuredType5");}
	;
	/*
	setType 	{outFile << toWrite("structuredType3");}
	*/
arrayType:
	ARRAY OF type 	{outFile << toWrite("arrayType1");} /*Dynamic Array*/
	|
	ARRAY LBRACKET rangeList RBRACKET OF type 	{outFile << toWrite("arrayType2");} /* Static Array*/
	;
rangeList:
	rangeWithIdentfier COMMA rangeList 	{outFile << toWrite("rangeList1");}
	|
	rangeWithIdentfier 	{outFile << toWrite("rangeList2");}
	;
rangeWithIdentfier:
	expr DOTDOT expr 	{outFile << toWrite("rangeWithIdentfier1");}
	|
	IDENTIFIER 	{outFile << toWrite("rangeWithIdentfier2");}
	;
recordType:
	RECORD END 	{outFile << toWrite("recordType1");}
	|
	RECORD fieldList END 	{outFile << toWrite("recordType2");}
	;
fieldList: /* find explanation page 40-41*/
	fixedFields 	{outFile << toWrite("fieldList1");}
	|
	fixedFields SEMICOLON 	{outFile << toWrite("fieldList2");}
	|
	variantPart 	{outFile << toWrite("fieldList3");}
	|
	variantPart SEMICOLON 	{outFile << toWrite("fieldList4");}
	|
	fixedFields SEMICOLON variantPart 	{outFile << toWrite("fieldList5");}
	|
	fixedFields SEMICOLON variantPart SEMICOLON 	{outFile << toWrite("fieldList6");}
	;
fixedFields:
	identifierList COLON type SEMICOLON fixedFields 	{outFile << toWrite("fixedFields1");}
	|
	identifierList COLON type SEMICOLON 	{outFile << toWrite("fixedFields2");}
	;
variantPart:
	CASE IDENTIFIER COLON ordinalType OF variantList 	{outFile << toWrite("variantPart1");}
	|
	CASE IDENTIFIER OF variantList 	{outFile << toWrite("variantPart2");}
	|
	CASE ordinalTypeWithoutIdentifier OF variantList 	{outFile << toWrite("variantPart3");}
	;
variantList:
	variant SEMICOLON 	{outFile << toWrite("variantList1");}
	|
	variant SEMICOLON variantList 	{outFile << toWrite("variantList2");}
	;
/* recheck this page 40-41*/
variant:
	caseCondList COLON LPAREN RPAREN 	{outFile << toWrite("variant1");}
	|
	caseCondList COLON LPAREN fieldList RPAREN 	{outFile << toWrite("variant2");}
	;
/*setType:
	SET OF ordinalType
	;*/
fileType:
	FILEP 	{outFile << toWrite("fileType1");}
	|
	FILEP OF type 	{outFile << toWrite("fileType2");}
	;
pointerType:
	CAP type 	{outFile << toWrite("pointerType1");}
	;
proceduralType:
	procOrFuncTypeHeader 	{outFile << toWrite("proceduralType1");}
	;
procOrFuncTypeHeader:
	functionTypeHeader 	{outFile << toWrite("procOrFuncTypeHeader1");}
	|
	procedureTypeHeader 	{outFile << toWrite("procOrFuncTypeHeader2");}
	;
functionTypeHeader:
	FUNCTION parameterList COLON type 	{outFile << toWrite("functionTypeHeader1");}
	;
procedureTypeHeader:
	PROCEDURE parameterList 	{outFile << toWrite("procedureTypeHeader1");}
	;
varDeclaration:
	identifierList COLON type SEMICOLON 	{outFile << toWrite("varDeclaration1");} 
	;
expr:
	simpleExpr {outFile << toWrite("expr1");}
	|
	simpleExpr op1 expr {outFile << toWrite("expr2");}
	;
simpleExpr:
	term   {outFile << toWrite("simpleExpr1");}
	|
	term addOp simpleExpr 	{outFile << toWrite("simpleExpr2");}
	;
term:
	factor {outFile << toWrite("term1");}
	|
	factor mulOp2 term 	{outFile << toWrite("term2");}
	;
factor:
	LPAREN expr RPAREN 	{outFile << toWrite("factor1");}
	|
	functionCall 	{outFile << toWrite("factor2");}
	|
	varReference 	{outFile << toWrite("factor3");}
	|
	unsignedConstant   {outFile<< toWrite("factor4");}
	|
	NOT factor 	{outFile << toWrite("factor5");}
	|
	sign factor 	{outFile << toWrite("factor6");}
	|
	valueTypecast 	{outFile << toWrite("factor8");}
	|
	addressFactor 	{outFile << toWrite("factor9");}
	|
	TRUE 	{outFile << toWrite("factor10");} /*true and false added by me for as:boolean = TRUE*/
	|
	FALSE 	{outFile << toWrite("factor11");}
	;/*
	|
	setConstructor
	*/
unsignedConstant:
	number 	{outFile << toWrite("unsignedConstant1");}
	|
	string {outFile<< toWrite("unsignedConstant2");}
	|
	NIL 	{outFile << toWrite("unsignedConstant3");}
	;
functionCall:
	functionCallPart1 actualParameterList 	{outFile << toWrite("functionCall1");}
	;
actualParameterList:
	LPAREN RPAREN  	{outFile << toWrite("actualParameterList1");}
	|
	LPAREN exprList RPAREN {outFile << toWrite("actualParameterList2");}
	;
exprList:
	expr COMMA exprList    {outFile << toWrite("exprList1");}
	|
	expr   {outFile << toWrite("exprList2");}
	;
functionCallPart1:
	fieldReference DOT IDENTIFIER 	 {outFile << toWrite("functionCallPart1-1");}
	|
	IDENTIFIER 	{outFile << toWrite("functionCallPart1-2");}
	;
	/*
setConstructor:
	LBRACKET RBRACKET 	{outFile << toWrite("setConstructor1");}
	|
	LBRACKET setGroupList RBRACKET 	{outFile << toWrite("setConstructor2");}
	;
setGroupList:
	setGroup COMMA setGroupList 	{outFile << toWrite("setGroupList1");}
	|
	setGroup 	{outFile << toWrite("setGroupList2");}
	;
setGroup:
	expr 	{outFile << toWrite("setGroup1");}
	|
	expr DOTDOT expr 	{outFile << toWrite("setGroup2");}
	;*/
valueTypecast:
	ordinalTypeWithoutIdentifier LPAREN expr RPAREN 	{outFile << toWrite("valueTypecast1");} /*Identifier taken care in functionCall*/	;
addressFactor:
	AT varReference 	{outFile << toWrite("addressFactor1");} /*IDENTIFIER can be procedure | function | qualified method */
	;
statement:
	openStatement 	{outFile << toWrite("statement1");}
	|
	closedStatement 	{outFile << toWrite("statement2");}
	;
closedStatement:
	compoundStatement 	{outFile << toWrite("closedStatement1");}
	|
	caseStatement 	{outFile << toWrite("closedStatement2");}
	|
	closedForStatement 	{outFile << toWrite("closedStatement3");}
	|
	repeatStatement 	{outFile << toWrite("closedStatement4");}
	|
	closedWhileStatement 	{outFile << toWrite("closedStatement5");}
	|
	closedWithStatement 	{outFile << toWrite("closedStatement6");}
	|
	assignmtStatement 	{outFile << toWrite("closedStatement7");}
	|
	procStatement 	{outFile << toWrite("closedStatement8");}
	|
	closedIfStatement 	{outFile << toWrite("closedStatement9");}
	|
	EXIT 	{outFile << toWrite("closedStatement10");}
	|
	BREAK	{outFile << toWrite("closedStatement11");}
	|
	CONTINUE	{outFile << toWrite("closedStatement12");}
	;
openStatement:
	openIfStatement 	{outFile << toWrite("openStatement1");}
    |
    openWithStatement 	{outFile << toWrite("openStatement2");}
    |
    openWhileStatement 	{outFile << toWrite("openStatement3");}
    |
    openForStatement 	{outFile << toWrite("openStatement4");}
	;
openIfStatement:
	IF expr THEN statement 	{outFile << toWrite("openIfStatement1");}
	|
	IF expr THEN closedStatement ELSE openStatement 	{outFile << toWrite("openIfStatement2");}
	;
closedIfStatement:
	IF expr THEN closedStatement ELSE closedStatement 	{outFile << toWrite("closedIfStatement1");}
	;
assignmtStatement:
	varReference /* IDENTIFIER can be function*/ CE expr 	{outFile << toWrite("assignmtStatement1");}
	;
procStatement:
	functionCall 	{outFile << toWrite("procStatement1");}
	|
	READ actualParameterList 	{outFile << toWrite("procStatement2");}
	|
	WRITE actualParameterList 	{outFile << toWrite("procStatement3");}
	|
	NEW allocParameterList 	{outFile << toWrite("procStatement4");}
	|
	DISPOSE allocParameterList 	{outFile << toWrite("procStatement5");}
	;
allocParameterList:
	LPAREN IDENTIFIER/*object Identifier*/ COMMA IDENTIFIER /*constructor name*/ LPAREN exprList RPAREN RPAREN {outFile << toWrite("allocParameterList1");}
	|
	LPAREN IDENTIFIER RPAREN 	{outFile << toWrite("allocParameterList2");}
	;
compoundStatement:
	BEGINP statementList END 	{outFile << toWrite("compoundStatement1");}
	;
statementList:
	statement SEMICOLON statementList  {outFile << toWrite("statementList1");}
	|
	/*epsilon*/    {outFile << toWrite("epsilon");}
	;
caseStatement:
    CASE expr OF caseList END 	{outFile << toWrite("caseStatement1");}
    |
    CASE expr OF caseList otherwisePart statementList END 	{outFile << toWrite("caseStatement2");}
    ;
caseList:
    caseStmt SEMICOLON caseList 	{outFile << toWrite("caseList1");}
	|
	caseStmt SEMICOLON 	{outFile << toWrite("caseList2");}
	;
caseStmt:
	caseCondList COLON statement 	{outFile << toWrite("caseStmt1");}
	;
caseCondList:
    caseCond COMMA caseCondList 	{outFile << toWrite("caseCondList1");}
	|
	caseCond 	{outFile << toWrite("caseCondList2");}
	;
caseCond:
	expr 	{outFile << toWrite("caseCond1");}
	|
	expr DOTDOT expr 	{outFile << toWrite("caseCond2");}
	;
otherwisePart:
	OTHERWISE COLON 	{outFile << toWrite("otherwisePart1");}
	;
openForStatement:
    openForToDowntoStatement 	{outFile << toWrite("openForStatement1");}
    |
    openForInStatement 	{outFile << toWrite("openForStatement2");}
    ;
closedForStatement:
	closedForToDowntoStatement 	{outFile << toWrite("closedForStatement1");}
	|
	closedForInStatement 	{outFile << toWrite("closedForStatement2");}
	;
closedForToDowntoStatement:
	FOR controlVariable CE initialValue TO finalValue DO closedStatement 	{outFile << toWrite("closedForToDowntoStatement1");}
	|
	FOR controlVariable CE initialValue DOWNTO finalValue DO closedStatement 	{outFile << toWrite("closedForToDowntoStatement2");}
	;
openForToDowntoStatement:
	FOR controlVariable CE initialValue TO finalValue DO openStatement 	{outFile << toWrite("openForToDowntoStatement1");}
	|
	FOR controlVariable CE initialValue DOWNTO finalValue DO openStatement 	{outFile << toWrite("openForToDowntoStatement2");}
	;
controlVariable:
	IDENTIFIER 	{outFile << toWrite("controlVariable1");} /*variable identifier*/
	;
initialValue:
	expr 	{outFile << toWrite("initialValue1");}
	;
finalValue:
	expr 	{outFile << toWrite("finalValue1");}
	;
openForInStatement:
	FOR controlVariable IN enumerable DO openStatement 	{outFile << toWrite("openForInStatement1");}
	;
closedForInStatement:
	FOR controlVariable IN enumerable DO closedStatement 	{outFile << toWrite("closedForInStatement1");}
	;
enumerable:
	enumType 	{outFile << toWrite("enumerable1");}
	|
	string 	{outFile << toWrite("enumerable2");}
	|
	LPAREN stringList RPAREN 	{outFile << toWrite("enumerable3");}
	;
stringList:
	string COMMA stringList 	{outFile << toWrite("stringList1");}
	|
	string 	{outFile << toWrite("stringList2");}
	;
repeatStatement:
	REPEAT statementList UNTIL expr 	{outFile << toWrite("repeatStatement1");}
	;
openWhileStatement:
	WHILE expr DO openStatement 	{outFile << toWrite("openWhileStatement1");}
	;
closedWhileStatement:
    WHILE expr DO closedStatement 	{outFile << toWrite("closedWhileStatement1");}
    ;
openWithStatement:
    WITH varReferenceList DO openStatement 	{outFile << toWrite("openWithStatement1");}
    ;
closedWithStatement:
	WITH varReferenceList DO closedStatement 	{outFile << toWrite("closedWithStatement1");}
	;
varReferenceList:
	varReference COMMA varReferenceList 	{outFile << toWrite("varReferenceList1");}
	|
	varReference 	{outFile << toWrite("varReferenceList2");} /* page 162 for allowed references*/
	;
procDeclaration:
	procedureHeader SEMICOLON subroutineBlock 	{outFile << toWrite("procDeclaration1");}
	|
	PROCEDURE IDENTIFIER DOT IDENTIFIER parameterList SEMICOLON subroutineBlockWithoutForward 	{outFile << toWrite("procDeclaration2");}
	;
subroutineBlockWithoutForward:
	initList compoundStatement SEMICOLON 	{outFile << toWrite("subroutineBlockWithoutForward1");}
	|
	externalDirective SEMICOLON 	{outFile << toWrite("subroutineBlockWithoutForward2");}
	;
procedureHeader:
	PROCEDURE IDENTIFIER /* identifier or qualified method identifier*/ parameterList 	{outFile << toWrite("procedureHeader1");}
	;
subroutineBlock:
	initList compoundStatement SEMICOLON 	{outFile << toWrite("subroutineBlock1");}
	|
	externalDirective SEMICOLON 	{outFile << toWrite("subroutineBlock2");}
	|
	FORWARD SEMICOLON 	{outFile << toWrite("subroutineBlock3");}
	;
funcDeclaration:
	functionHeader SEMICOLON subroutineBlock 	{outFile << toWrite("funcDeclaration1");}
	|
	FUNCTION IDENTIFIER DOT IDENTIFIER parameterList COLON type SEMICOLON subroutineBlockWithoutForward 	{outFile << toWrite("funcDeclaration2");}
	;
functionHeader:
	FUNCTION IDENTIFIER /* identifier or qualified method identifier*/ parameterList COLON type 	{outFile << toWrite("functionHeader1");}
	;
parameterList:
	LPAREN RPAREN 	{outFile << toWrite("parameterList1");}
	|
	LPAREN parameterDeclarationList RPAREN 	{outFile << toWrite("parameterList2");}
	;
parameterDeclarationList:
	parameterDeclaration SEMICOLON parameterDeclarationList 	{outFile << toWrite("parameterDeclarationList1");}
	|
	parameterDeclaration 	{outFile << toWrite("parameterDeclarationList2");}
	;
parameterDeclaration:
	valueParameter 	{outFile << toWrite("parameterDeclaration1");}
	|
	variableParameter 	{outFile << toWrite("parameterDeclaration2");}
	|
	outParameter 	{outFile << toWrite("parameterDeclaration3");}
	|
	constantParameter 	{outFile << toWrite("parameterDeclaration4");}
	;
valueParameter:
	identifierList COLON parameterType 	{outFile << toWrite("valueParameter1");}
	|
	identifierList COLON ARRAY OF parameterType 	{outFile << toWrite("valueParameter2");}
	;/*
	IDENTIFIER COLON parameterType EQUAL defaultParameterValue
	;*/
parameterType:
	ordinalType 	{outFile << toWrite("parameterType1");}
	|
	STRING 	{outFile << toWrite("parameterType2");}
	|
	realType 	{outFile << toWrite("parameterType3");}
	;
variableParameter:
	VAR identifierList 	{outFile << toWrite("variableParameter1");}
	|
	VAR identifierList COLON parameterType 	{outFile << toWrite("variableParameter2");}
	|
	VAR identifierList COLON ARRAY OF parameterType 	{outFile << toWrite("variableParameter3");}
	;
outParameter:
	OUT identifierList 	{outFile << toWrite("outParameter1");}
	|
	OUT identifierList COLON parameterType 	{outFile << toWrite("outParameter2");}
	|
	OUT identifierList COLON ARRAY OF parameterType 	{outFile << toWrite("outParameter3");}
	;
constantParameter:
	CONST identifierList 	{outFile << toWrite("constantParameter1");}
	|
	CONST identifierList COLON parameterType 	{outFile << toWrite("constantParameter2");}
	|
	CONST identifierList COLON ARRAY OF parameterType 	{outFile << toWrite("constantParameter3");}
	;
	/*
	CONST IDENTIFIER COLON IDENTIFIER EQUAL defaultParameterValue
	;
defaultParameterValue:
	expr
	;*/
externalDirective:
	EXTERNAL 	{outFile << toWrite("externalDirective1");}
	|
	EXTERNAL string 	{outFile << toWrite("externalDirective2");}
	|
	EXTERNAL string NAME string 	{outFile << toWrite("externalDirective3");}
	|
	EXTERNAL string INDEX number 	{outFile << toWrite("externalDirective4");}
	;
varReference:
	nonPointerReference 	{outFile << toWrite("varReference1");}
	|
	pointerReference 	{outFile << toWrite("varReference2");}
	;
nonPointerReference:
	indexedReference 	{outFile << toWrite("nonPointerReference1");}
	|
	fieldReference DOT pointerReference 	{outFile << toWrite("nonPointerReference2");}
	;
pointerReference:
	pointerReference CAP 	{outFile << toWrite("pointerReference1");}
	|
	IDENTIFIER 	{outFile << toWrite("pointerReference2");}
	;
indexedReference:
	arrayReference 	{outFile << toWrite("indexedReference1");}
	|
	arrayReference DOT fieldReference 	{outFile << toWrite("indexedReference2");}
	|
	fieldReference DOT arrayReference 	{outFile << toWrite("indexedReference3");}
	|
	fieldReference DOT arrayReference DOT fieldReference 	{outFile << toWrite("indexedReference4");}
	;
arrayReference:
	IDENTIFIER LBRACKET exprList RBRACKET 	{outFile << toWrite("arrayReference1");}
	;
fieldReference:
	fieldReference DOT pointerReference 	{outFile << toWrite("fieldReference1");}
	|
	pointerReference 	{outFile << toWrite("fieldReference2");}
	;
operatorDeclaration:
	OPERATOR assignmtDefinition 	{outFile << toWrite("operatorDeclaration1");}
	|
	OPERATOR arithmeticDefinition 	{outFile << toWrite("operatorDeclaration2");}
	|
	OPERATOR comparisonDefinition 	{outFile << toWrite("operatorDeclaration3");}
	;
assignmtDefinition:
	CE LPAREN IDENTIFIER/*argument variable*/ COLON parameterType /*type*/ RPAREN IDENTIFIER/*return Variable*/ COLON parameterType /*return type*/ SEMICOLON initList compoundStatement SEMICOLON 	{outFile << toWrite("assignmtDefinition1");}
	;
arithmeticDefinition:
	op2 LPAREN parameterList2 RPAREN IDENTIFIER COLON parameterType SEMICOLON initList compoundStatement SEMICOLON 	{outFile << toWrite("arithmeticDefinition1");}
	;
comparisonDefinition:
	compOp LPAREN parameterList2 RPAREN IDENTIFIER COLON BOOLEAN SEMICOLON initList compoundStatement SEMICOLON 	{outFile << toWrite("comparisonDefinition1");}
	;
parameterList2:
	IDENTIFIER COMMA IDENTIFIER COLON parameterType 	{outFile << toWrite("parameterList2-1");}
	|
	IDENTIFIER COLON parameterType SEMICOLON IDENTIFIER COLON parameterType 	{outFile << toWrite("parameterList2-2");}
	|
	IDENTIFIER COLON parameterType 	{outFile << toWrite("parameterList2-3");}
	;
objectType:
        OBJECT componentList END        {outFile << toWrite("objectType1"); /*new*/}
        ;
componentList:
        component componentList         {outFile << toWrite("componentList1"); /*new*/}
        |
        component       {outFile << toWrite("componentList2"); /*new*/}
        ;
component:
        visibilitySpecifier fieldOrMethodDefinitionList         {outFile << toWrite("component1"); /*new*/}
        ;
fieldOrMethodDefinitionList:
        fieldOrMethodDefinition fieldOrMethodDefinitionList     {outFile << toWrite("fieldOrMethodDefinitionList1"); /*new*/}
        |
        fieldOrMethodDefinition         {outFile << toWrite("fieldOrMethodDefinitionList2"); /*new*/}
        ;
fieldOrMethodDefinition:
        fieldDefinition         {outFile << toWrite("fieldOrMethodDefinition1"); /*new*/}
        |
        methodDefinition        {outFile << toWrite("fieldOrMethodDefinition2"); /*new*/}
        ;
fieldDefinition:
        identifierList COLON type SEMICOLON     {outFile << toWrite("fieldDefinition1"); /*new*/}
        ;
constructorDeclaration:
        CONSTRUCTOR IDENTIFIER DOT IDENTIFIER parameterList SEMICOLON subroutineBlock   {outFile << toWrite("constructorDeclaration1"); /*new*/}
        ;
destructorDeclaration:
        DESTRUCTOR IDENTIFIER DOT IDENTIFIER parameterList SEMICOLON subroutineBlock    {outFile << toWrite("destructorDeclaration1"); /*new*/}
        ;
constructorHeader:
        CONSTRUCTOR IDENTIFIER /* can be a qualified method identfier*/ parameterList   {outFile << toWrite("constructorHeader1"); /*new*/}
        ;
destructorHeader:
        DESTRUCTOR IDENTIFIER /* can be a qualified method identfier*/ parameterList    {outFile << toWrite("destructorHeader1"); /*new*/}
        ;
methodDefinition:
        methodDefinitionHeader SEMICOLON        {outFile << toWrite("methodDefinition1"); /*new*/}
        ;
methodDefinitionHeader:
        functionHeader  {outFile << toWrite("methodDefinitionHeader1"); /*new*/}
        |
        procedureHeader         {outFile << toWrite("methodDefinitionHeader2"); /*new*/}
        |
        constructorHeader       {outFile << toWrite("methodDefinitionHeader3"); /*new*/}
        |
        destructorHeader        {outFile << toWrite("methodDefinitionHeader4"); /*new*/}
        ;
visibilitySpecifier:
        PRIVATE         {outFile << toWrite("visibilitySpecifier1"); /*new*/}
        |
        PROTECTED       {outFile << toWrite("visibilitySpecifier2"); /*new*/}
        |
        PUBLIC  {outFile << toWrite("visibilitySpecifier3"); /*new*/}
        ;
compOp:
        EQUAL   {outFile << toWrite("compOp1"); /*new*/}
        |
        LESS    {outFile << toWrite("compOp2"); /*new*/}
        |
        GREATER         {outFile << toWrite("compOp3"); /*new*/}
        |
        LE      {outFile << toWrite("compOp4"); /*new*/}
        |
        GE      {outFile << toWrite("compOp5"); /*new*/}
        |
        IN      {outFile << toWrite("compOp6"); /*new*/}
        ;
op2:
        PLUS    {outFile << toWrite("op2-1"); /*new*/}
        |
        MINUS   {outFile << toWrite("op2-2"); /*new*/}
        |
        MUL     {outFile << toWrite("op2-3"); /*new*/}
        |
        DIVIDE  {outFile << toWrite("op2-4"); /*new*/}
        |
        SS      {outFile << toWrite("op2-5"); /*new*/}
        ;
mulOp2:
        MUL     {outFile << toWrite("mulOp2-1");}
        |
        DIVIDE  {outFile << toWrite("mulOp2-2");}
        |
        DIV     {outFile << toWrite("mulOp2-3");}
        |
        MOD     {outFile << toWrite("mulOp2-4");}
        |
        AND     {outFile << toWrite("mulOp2-5");}
        |
        AS              {outFile << toWrite("mulOp2-6");}
        |
        SHL     {outFile << toWrite("mulOp2-7");}
        |
        SHR     {outFile << toWrite("mulOp2-8");}
        |
        LL      {outFile << toWrite("mulOp2-9"); /*new*/}
        |
        GG      {outFile << toWrite("mulOp2-10"); /*new*/}
        ;
op1:
        LE      {outFile << toWrite("op1-1");} /*added by me `MUL` was here too*/
        |
        GREATER         {outFile << toWrite("op1-2");}
        |
        GE      {outFile << toWrite("op1-3");}
        |
        LESS    {outFile << toWrite("op1-8"); /*new*/}
        |
        EQUAL   {outFile << toWrite("op1-4");}
        |
        LG              {outFile << toWrite("op1-5");}
        |
        IN              {outFile << toWrite("op1-6");}
        |
        IS              {outFile << toWrite("op1-7");}
        ;
addOp:
        PLUS    {outFile << toWrite("addOp1");}
        |
        MINUS   {outFile << toWrite("addOp2");}
        |
        OR              {outFile << toWrite("addOp3");}
        |
        XOR     {outFile << toWrite("addOp4");}
        ;
%%
void yyerror(char *s) {
     fprintf(stderr, "%s at line:%d \n",s,lineNo);
}

int main(int argv, char *argc[]){
    lineNo = 1;
	for (int i=0;i<1000;i++)
		varCountArr[i] = 0;
	yyin = fopen(argc[1],"r");
	if(yyin == NULL){
		printf("unable to open file %s\n",argc[1] );
		return 0;
	}
	int i;
	char outFileName[105];
	char logFileName[105];
	for(i=0;(argc[1][i] != '\0') && (argc[1][i] != '.') && (i < 100) ;i++){
		outFileName[i] = argc[1][i];
		logFileName[i] = argc[1][i];
	}
	outFileName[i] = '\0';
	logFileName[i] = '\0';
	strcat(outFileName,".dot\0");	
	strcat(logFileName,".log\0");
	outFile.open(outFileName, ios::out | ios::trunc);
	if(!outFile.is_open()){
		printf("unable to open file %s\n",outFileName );
		return 0;
	}
	outFile << "digraph {rankdir=TB;label=\"Parse Tree\";\nsplines=\"line\";ordering=\"out\"; \n";
	//yydebug = 1;
	yyparse();
    outFile << (count-1) << "[label=\"File\"]\n";
	fclose(yyin);
	outFile << "}\n";
	outFile.close();
	return 0;
}
