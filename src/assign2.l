Letter [a-zA-Z]
DIGIT [0-9]
AN [a-zA-Z0-9_]
space [ \n\t]

%{
#include "stdio.h"
#include "stdlib.h"
#include "iostream"
#include "string"
#include "assign2.tab.h"
#include "assign2.h"
#define CS_LENGTH_LIMIT 1023
#define CS_MAX_ALLOWED 3
#define N 100000
void yyerror(char *);
int unexpectedEOF();

int ERROR_STATE;
int countLines(){
	int i;
	for(i=0;i<yyleng;i++){
		if(yytext[i] == '\n')
			lineNo++;
	}
	return 0;
}

int rewriteString(char *a,int len,int *errorLen){
	char* newString = new char[len];
	char buffer[1000];
	bool isCS = 1;
	char tempInt[CS_MAX_ALLOWED];
	int state = 1,intLen=0,newLen = 0,i = 0;
	for(i=0;i<len;i++){
		if(a[i] != '\'' && !isCS){
			newString[newLen] = a[i];
			newLen++;
		}
		else if(a[i] == '#' && isCS ){
			state = 2;
			if(intLen > 0){
				tempInt[intLen] = '\0';
				newString[newLen] = (char)atoi(tempInt);
				intLen = 0;
				newLen++;
			}
		}
		else if(a[i] == '\''){
			isCS = !isCS;
			if(state == 2){
				tempInt[intLen] = '\0';
				newString[newLen] = (char)atoi(tempInt);
				intLen = 0;
				newLen++;
			}
			state = 1;
			if(a[i-1] == '\'' && !isCS){
				newString[newLen] = '\'';
				newLen++;
			}
		}
		else if(state == 2){
			if(intLen >= CS_MAX_ALLOWED){
				*errorLen = i-CS_MAX_ALLOWED-1;
				fprintf(stderr,"Control String numeral out of bounds at position:%d at lineNo:%d",*errorLen,lineNo);
				return CS_LENGTH_LIMIT;
			}
			tempInt[intLen] = a[i];
			intLen++;
		}
	}
	if(state == 2){
		tempInt[intLen] = '\0';
		newString[newLen] = (char)atoi(tempInt);
		intLen = 0;
		newLen++;
	}
	yytext = newString;
	yyleng = newLen;
	yytext[yyleng] = '\0';
	return 0;
}

%}

%%

"(*"([^*]|\*+[^*)])*\*+")" {countLines();}
"{"[^"}"]*"}" {countLines();}
"//".*   {;}

(\'[^']\'|\#[0-9]+)  rewriteString(yytext,yyleng,&ERROR_STATE); yylval.strVal = strdup(yytext); return CONST_CHAR;
(\'[^']*\'|\#[0-9]+)+  {rewriteString(yytext,yyleng,&ERROR_STATE); yylval.strVal = strdup(yytext); countLines(); return STR;}

(?i:end{space}*".") return END_OF_PROGRAM;


":=" 	 return CE;
"<>" 	 return LG;
"<<" 	 return LL;
">>" 	 return GG;
">=" 	 return GE;
"<=" 	 return LE;
"**" 	 return SS;
"+" 	 return PLUS;
"-" 	 return MINUS;
"*" 	 return MUL;
"/" 	 return DIVIDE;
"=" 	 return EQUAL;
"<" 	 return LESS;
">" 	 return GREATER;
":" 	 return COLON;
";" 	 /*fprintf(stderr,"\nFound Semicolon at lineNo:%d ",lineNo);*/ return SEMICOLON;
"," 	 return COMMA;
"(" 	 return LPAREN;
")" 	 return RPAREN;
"[" 	 return LBRACKET;
"]" 	 return RBRACKET;
"^" 	 return CAP;
".." 	 return DOTDOT;
"." 	 return DOT;
"@" 	 return AT;

"$"[0-9a-fA-F]+	{yylval.intVal = strtol(yytext+1,NULL,16); return HEXADECIMAL;}
"&"[0-7]+		{yylval.intVal = strtol(yytext+1,NULL,8); return OCTAL;}
"%"[01]+        {yylval.intVal = strtol(yytext+1,NULL,2); return BINARY;}
{DIGIT}+"."?{DIGIT}*[eE][+-]?{DIGIT}+ {yylval.floatVal = atof(yytext); return EXPONENTIAL;}
{DIGIT}+"."{DIGIT}+	     {yylval.floatVal = atof(yytext); return FLOAT;}
{DIGIT}+"."/[^.]	   {yylval.floatVal = atof(yytext); return FLOAT;}
{DIGIT}+   {yylval.intVal = atol(yytext); return INT;}

(?i:char) 		return CHAR;
(?i:absolute) 	 return ABSOLUTE;
(?i:and) 	 return AND;
(?i:array) 	 return ARRAY;
(?i:asm) 	 return ASM;
(?i:begin) 	 return BEGINP;
(?i:case) 	 return CASE;
(?i:const) 	 return CONST;
(?i:constructor) 	 return CONSTRUCTOR;
(?i:destructor) 	 return DESTRUCTOR;
(?i:div) 	 return DIV;
(?i:do) 	 return DO;
(?i:downto) 	 return DOWNTO;
(?i:else) 	 return ELSE;
(?i:end) 	 return END;
(?i:file) 	 return FILEP;
(?i:for) 	 return FOR;
(?i:function) 	 return FUNCTION;
(?i:goto) 	 return GOTO;
(?i:if) 	 return IF;
(?i:implementation) 	 return IMPLEMENTATION;
(?i:in) 	 return IN;
(?i:inherited) 	 return INHERITED;
(?i:inline) 	 return INLINE;
(?i:interface) 	 return INTERFACE;
(?i:label) 	 return LABEL;
(?i:mod) 	 return MOD;
(?i:nil) 	 return NIL;
(?i:not) 	 return NOT;
(?i:object) 	 return OBJECT;
(?i:nested) return NESTED;
(?i:of) 	 return OF;
(?i:operator) 	 return OPERATOR;
(?i:or) 	 return OR;
(?i:procedure) 	 return PROCEDURE;
(?i:program) 	 return PROGRAM;
(?i:record) 	 return RECORD;
(?i:reintroduce) 	 return REINTRODUCE;
(?i:repeat) 	 return REPEAT;
(?i:self) 	 return SELF;
(?i:set) 	 return SET;
(?i:shl) 	 return SHL;
(?i:shr) 	 return SHR;
(?i:string) 	 return STRING;
(?i:then) 	 return THEN;
(?i:to) 	 return TO;
(?i:type) 	 return TYPE;
(?i:unit) 	 return UNIT;
(?i:until) 	 return UNTIL;
(?i:uses) 	 return USES;
(?i:var) 	 return VAR;
(?i:while) 	 return WHILE;
(?i:with) 	 return WITH;
(?i:xor) 	 return XOR;
(?i:dispose) 	 return DISPOSE;
(?i:false) 	 return FALSE;
(?i:true) 	 return TRUE;
(?i:exit) 	 return EXIT;
(?i:new) 	 return NEW;
(?i:as) 	 return AS;
(?i:class) 	 return CLASS;
(?i:dispinterface) 	 return DISPINTERFACE;
(?i:except) 	 return EXCEPT;
(?i:exports) 	 return EXPORTS;
(?i:finalization) 	 return FINALIZATION;
(?i:finally) 	 return FINALLY;
(?i:initialization) 	 return INTIALIZATION;
(?i:is) 	 return IS;
(?i:library) 	 return LIBRARY;
(?i:on) 	 return ON;
(?i:out) 	 return OUT;
(?i:bitpacked) 	 return BITPACKED;
(?i:properly) 	 return PROPERLY;
(?i:raise) 	 return RAISE;
(?i:resourcestring) 	 return RESOURCESTRING;
(?i:threadvar) 	 return THREADVAR;
(?i:try) 	 return TRY;
(?i:abstract) 	 return ABSTRACT;
(?i:alias) 	 return ALIAS;
(?i:assembler) 	 return ASSEMBLER;
(?i:packed) 	 return PACKED;
(?i:break) 	 return BREAK;
(?i:cdecl) 	 return CDECL;
(?i:continue) 	 return CONTINUE;
(?i:cppdecl) 	 return CPPDECL;
(?i:cvar) 	 return CVAR;
(?i:default) 	 return DEFAULT;
(?i:deprecated) 	 return DEPRECATED;
(?i:dynamic) 	 return DYNAMIC;
(?i:enumerator) 	 return ENUMERATOR;
(?i:experimental) 	 return EXPERIMENTAL;
(?i:export) 	 return EXPORT;
(?i:external) 	 return EXTERNAL;
(?i:far) 	 return FAR;
(?i:far16) 	 return FAR16;
(?i:forward) 	 return FORWARD;
(?i:generic) 	 return GENERIC;
(?i:helper) 	 return HELPER;
(?i:implements) 	 return IMPLEMENTS;
(?i:index) 	 return INDEX;
(?i:interrupt) 	 return INTERRUPT;
(?i:iochecks) 	 return IOCHECKS;
(?i:local) 	 return LOCAL;
(?i:message) 	 return MESSAGE;
(?i:name) 	 return NAME;
(?i:near) 	 return NEAR;
(?i:nodefault) 	 return NODEFAULT;
(?i:noreturn) 	 return NORETURN;
(?i:nostackframe) 	 return NOSTACKFRAME;
(?i:oldfpccall) 	 return OLDFPCCALL;
(?i:otherwise) 	 return OTHERWISE;
(?i:overload) 	 return OVERLOAD;
(?i:override) 	 return OVERRIDE;
(?i:pascal) 	 return PASCAL;
(?i:platform) 	 return PLATFORM;
(?i:private) 	 return PRIVATE;
(?i:protected) 	 return PROTECTED;
(?i:public) 	 return PUBLIC;
(?i:published) 	 return PUBLISHED;
(?i:read) 	 return READ;
(?i:register) 	 return REGISTER;
(?i:result) 	 return RESULT;
(?i:safecall) 	 return SAFECALL;
(?i:saveregisters) 	 return SAVEREGISTERS;
(?i:softfloat) 	 return SOFTFLOAT;
(?i:specialize) 	 return SPECIALIZE;
(?i:static) 	 return STATIC;
(?i:stdcall) 	 return STDCALL;
(?i:stored) 	 return STORED;
(?i:strict) 	 return STRICT;
(?i:unalligned) 	 return UNALLIGNED;
(?i:unimplemented) 	 return UNIMPLEMENTED;
(?i:varargs) 	 return VARARGS;
(?i:virtual) 	 return VIRTUAL;
(?i:write) 	 return WRITE;
(?i:integer) 	 return INTEGER;
(?i:cardinal) 	 return CARDINAL;
(?i:shortint) 	 return SHORTINT;
(?i:smallint) 	 return SMALLINT;
(?i:longint) 	 return LONGINT;
(?i:int64) 	 return INT64;
(?i:byte) 	 return BYTE;
(?i:longword) 	 return LONGWORD;
(?i:word) 	 return WORD;
(?i:extended) 	 return EXTENDED;
(?i:boolean) 	 return BOOLEAN;
(?i:real) 	 return REAL;



[a-zA-Z_]{AN}*   {return IDENTIFIER;}

\n  		lineNo++;

[ \t]    

["&""$""%"]?{DIGIT}+[a-zA-Z_]{AN}* fprintf(stderr,"\n\"%s\"  is BADIDENTIFIER at lineNo:%d ",yytext,lineNo);	return BADIDENTIFIER;

.            fprintf(stderr,"\n\"%s\" is UNKNOWN at lineNo:%d ",yytext,lineNo); return ALIEN;
%%
int unexpectedEOF(){
	fprintf(stderr, "unexpected EOF inside comment at line %d\n",lineNo );
	exit(1);
}

int yywrap(void){
	return 1;
}
