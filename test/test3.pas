program forLoop;
procedure for1();
var
   a: integer;
begin
   for a := 10  to 20 do
   begin
      writeln('value of a: ', a);
   end;
end;

procedure while1();
var
   a: integer;
begin
   a := 10;
  (* while loop execution *)
   while  a < 20 do
   begin
      writeln('value of a: ', a);
      a:=a +1;
      if( a > 15) then
         (* terminate the loop using break statement *)
          break;
    end;
end;


procedure nestedPrime();
var
   i, j:integer;
begin
   for i := 2 to 50 do
   begin
      for j := 2 to i do
         if (i mod j)=0  then
            break; {* if factor found, not prime *}
      if(j = i) then
         writeln(i , ' is prime' );
   end;
end;

procedure repeatUntilLoop();
var
   a: integer;
begin
   a := 10;
   (* repeat until loop execution *)
   repeat
      writeln('value of a: ', a);
      a := a + 1;
   until a = 20;
end;

var
   a: integer;
begin
   a := 10;
   (* repeat until loop execution *)
   repeat
      if( a = 15) then
      begin
         (* skip the iteration *)
         a := a + 1;
         continue;
      end;
      writeln('value of a: ', a);
      a := a+1;
   until ( a = 20 );
end.