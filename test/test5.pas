program exRecursion;
var
  num2,num3:integer;
  extn : extended;
  bool : Boolean;
  ch: char;
  str_1: string[(10 + 2 ) * 2];
  array1 : array [2..4,34..56,23..30] of integer;
  array2 : array [2..4,34..56,2..300] of integer;
  type
  enum1 = (das,das22,dsad,sad:=32*323+32,sads,sdads,ad);
  var
  array3: array [3..4] of ^integer;
  array4  : array of ^integerArray;
  array5 :array [1..6] of integer;
  string2:string;
  color: (red, blue, yellow, green, white, black, orange);
  record1 : array [1..4] of record
   id: integer;
   info: integer;
   next: integer;
end; 
var
  int1:integer;
type
  integerArray = array [1..6] of integer;


  procedure new22(x:integer; f:string); forward;
  procedure new23(x:integer; f:string); forward;
  procedure new24(x:integer; f:string); forward;
  procedure new25(x:integer; f:string); forward;

procedure new22(x:integer; f:string);
   var int4: integer;
   var new1:integer;
   function fact(x: integer;var str1:string): integer;
   procedure inFunction();
   type
    cellPtr = ^cell;
    intptr = ^integer;
    cell = record
       id: integer;
       info: integerArray;
       next: cellPtr;
    end;

Operator * (r : real; z1 : complex) z : complex;
begin
z.re := z1.re * r;
z.im := z1.im * r;
end;

   var
  list, newrec: cellPtr;
  count, classNum: integer;

  const
    size = 67;
    sentinel = NIL;

    begin
    color1 := orange;
    writeln('In inFunction()');
    end;

function funcInFunc(x:integerArray; y: array of integer ): integer;
begin
   writeln('funcInFunc');
end;
    begin
       if x=0 then
       begin
          fact := 1;
          end
       else
       begin
          fact := x * fact(x-1,x);
          end;
       inFunction();
       funcInFunc(var1,var2);
    end;

   procedure nested1();
      var int2:integer;
      begin
         int2:=45;
         writeln('In Nested ',df);
      end;

   begin
      as22:=3;
      writeln('In New ',as22);
      nested1();
      new1 := fact(x,ff);
      writeln(' Factorial ', x, ' is: ' , new1);
   end;
(* this is * ** (*
a multiline comment *)

{ one more @#$%^#$%^*()_)_+_++__+ 
multiline comment 
{{

}
operator := (aRight: TTest1) Res: TTest2;
begin
Writeln('Implicit TTest1 => TTest2');
Res.f := IntToStr(aRight.f);
end;
operator := (aRight: TTest1) Res: TTest3;
begin
Writeln('Implicit TTest1 => TTest3');
Res.f := aRight.f <> 0;
end;
var
   num,num6: integer;
   cafasf: 23..34;
   boosad : Boolean ;
const
   charOR = 'Z' + 'A';

type
  num4 = integer;
  RPoint = Record
      Case num4 of
      False : (X,Y,Z : Real;);
      True : (R,theta,phi : Real;);
      end;
operator = (z1, z2 : complex) b : boolean;
Var
C1,C2 : Complex;
begin
If C1=C2 then
Writeln('C1 and C2 are equal');
end;

begin
   asd[2] := 34;
   awwsd[1].next := 43;
   num := 3+4/4-(3+48)*34-34+(gh or mb) + yug * (iyh- yg) + fad(kad,dad,dsasdasd);
   write(boosad);
   writeln(charOR);
   writeln(cafasf);
   ff:='asdd';
   new22(num,ff);
   if( a = 20 ) then
   begin
      writeln('a is less than 20' );
      end;

if( a = 20 ) then
   begin
      writeln('a is less than 20' );
      end;

      a := 20;

    if( a = 20 ) then
   begin
      writeln('a is less than 20' );
      end
   else if( a = 20 and a = 45) then
   begin
       writeln('a is not less than 20' );
       end;

    a := 344;

if( a <= 20 ) then
   begin
      writeln('a is less than 20' );
      end
   else if( a > 20 and a < 45) then
   begin
       writeln('a is not less than 20' );
       end
    else if( a >= sad and a <= 22) then
    begin
      writeln('value of a is : ', a);
      end;
  
   begin
   string2 := 'Some Random
   multiline String with 
   '#34': it is quote';
   ch := '\n';
   for d in (TWeekday,sadas,sad,asd,sad,ads) do
    writeln(d);
     for d in ('TWeekday','sadas','sad') do
     begin
    writeln(d);
    Break;
    Continue;
    EXIT;
    end;
    For Day := Monday to Friday do WOdsa();
      For I := 100 downto 1 do
        WriteLn ('Counting down : ',i);
      For I := 1 to 7*dwarfs do sDwarf(i);
   end;


   Case SEL of
  1 : Begin
       Writeln('Are you able to create');
       Writeln('a game of yourself using pascal??');
       Delay(2000);
      End;
  2 : Begin
       Writeln('Ahhh... no saved games');
       Delay(2000);
      End;
  3 : Begin
       Writeln('networking or 2 players?');
       Delay(2000);
      End;
  (4 + 3)*2 - 2: Begin
       Writeln('Exit?');
       YN := Readkey;
       Case YN of
        'y' : Begin
               Writeln('Nooooooooooooo...');
               Delay(1000);
              End;
        'n' : sadad();
       End;
      End;
 End;

end.
